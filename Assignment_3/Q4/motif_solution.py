#!/usr/bin/python

import sys
import numpy as np

# Idea for this code was received from Stanford,
# though I did take the time to undestand what was
# going on

def find_motif(x, y):
    curr = -1
    subseqindex = []
    # iterate through y
    for nucleotide in y:
	# find first instanceof nucleotide in x and 
	# increment curr by the index position + 2
        curr += x.index(nucleotide) + 2
	# put curr into list
        subseqindex.append(curr)
	# x shortened
        x= x[x.index(nucleotide) + 2::]
    # put list into strings
    return ' '.join(map(str, subseqindex))

        
sys.stdin.readline()
y = ''
while(True):
    line = sys.stdin.readline().strip()
    if line[0] == '>': break
    y += line

x = ''
line = sys.stdin.readline().strip()
while len(line) != 0:
    x += line
    line = sys.stdin.readline().strip()
 
motif = find_motif(y,x)
for m in motif:
    sys.stdout.write(str(m))





