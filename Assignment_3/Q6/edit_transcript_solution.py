#!/usr/bin/python

import sys
import numpy as np

def back_trace(D, x, y):
    # back trace to find edit strings for both x & y
    xa = ''
    ya = ''
    i = len(x)
    j = len(y)
    # terminate if we get to first row
    while i > 0:
        horz, vert, diag = sys.maxint, sys.maxint, sys.maxint
	delt = None
        # get values from previous cells
	# (upper left, upper, left)
        if i > 0 and j > 0:
            delt = 0 if x[i-1] == y[j-1] else 1
            diag = delt + D[i-1,j-1]
        if i > 0:
            vert = D[i-1,j] + 1
        if j > 0:
            horz = D[i,j-1] + 1
	# logic for inserting characters
        if diag < horz and diag < vert:
	    letter = 'R' if delt == 1 else 'M'
            xa += x[i-1]
            ya += y[j-1]
	    i = i - 1 
            j = j - 1
        elif vert <= horz:
            xa += x[i-1]
            ya += '-'
	    i = i - 1
	else:
            xa += '-'
            ya += y[j-1]
            j = j - 1
    # return strings in reversed order
    return ''.join(reversed(xa)), ''.join(reversed(ya))
	    
sys.stdin.readline()
t = ''
while(True):
    line = sys.stdin.readline().strip()
    if line[0] == '>': break
    t += line

p = ''
line = sys.stdin.readline().strip()
while len(line) != 0:
    p += line
    line = sys.stdin.readline().strip()

# Dynamic programming method to acquire 
# edit distance matrix
D = np.zeros((len(p) + 1, len(t) + 1), dtype=int)

D[0, 1:] = range(1, len(t) + 1)
D[1:, 0] = range(1, len(p) + 1)

for i in xrange(1, len(p) + 1):
    for j in xrange(1, len(t) + 1):
        delt = 1 if p[i-1] != t[j-1] else 0
        D[i,j] = min(D[i-1,j-1]+delt, D[i-1,j]+1,D[i,j-1]+1)

len_x = len(p)+1
len_y = len(t)+1
print D[-1,-1]
x_trace, y_trace = back_trace(D,p,t)
print y_trace
print x_trace

    
