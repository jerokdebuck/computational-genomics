#!/usr/bin/python
import sys
import itertools

def permutation_sequence(n):
   # create a list containing [1, 2, 3, ..., n]
   seq = [(i+1) for i in range(n)] 
   # use itertools module to get a list of permutations
   seq_list = list(itertools.permutations(seq))
   # print permutations
   sys.stdout.write(str(len(seq_list)) + "\n")
   for seqs in seq_list:
       for s in seqs:
           sys.stdout.write(str(s) + ' ')
       sys.stdout.write("\n")
    
value = sys.stdin.readline()
isInt = True
isInRange = True
try:
   value  = int(value)
except:
    isInt = False
    print 'Not an integer'

if isInt == True:
    if value < 1 or value > 7:
        isInRange = False
        print 'Value not in [1,7] range'

if isInt == True and isInRange == True:
    permutation_sequence(value)
else:
    print 'Could not perform operation'
