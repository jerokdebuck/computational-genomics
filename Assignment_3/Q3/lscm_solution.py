#!/usr/bin/python

import numpy as np
import sys

# This will take a while....
def longest_substring(dat):
    substr = ''
    if len(dat) > 1 and len(dat[0]) > 0:
	# loop through all characters in first string
        for i in range(len(dat[0])):
            for j in range(len(dat[0])-i+1):
		# if substring is longer than previous
		# and substring in data[0]][i->i+j] 
		# matches (i.e. is a substring)
		# then set substr to that substring
                if j > len(substr) and all(dat[0][i:i+j] in x for x in dat):
                    substr = dat[0][i:i+j]
    return substr



strings = []
seq = ''
line = sys.stdin.readline()
while (True):
    if len(line) == 0: break
    elif line[0] == '>':
        line = sys.stdin.readline().strip()
        while line[0] != '>':
            seq += line
            line = sys.stdin.readline().strip()
            if len(line) == 0:
                break
        strings.append(seq)
        seq = ''
substring = longest_substring(strings)
print substring

