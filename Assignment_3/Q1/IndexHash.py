#!/usr/bin/python
import sys

# Inverted substring index using hash table. 
# class IndexHash from Ben Langmead's github repository
class IndexHash(object):

    def __init__(self, t, ln, ival):
        self.t = t
        self.ln =ln
        self.ival = ival
        self.index = {}
        for i in xrange(0, len(t) - ln + 1):
            substr = t[i:i+ln] # extract substrings of length ln (6 for this problem)
            if substr in self.index:
                self.index[substr].append(i)
            else:
                self.index[substr] = [i]
    def query(self, p):
         return self.index.get(p, [])

