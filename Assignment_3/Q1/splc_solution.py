#!/usr/bin/python

import sys
import IndexHash
import collections

# find the amino acid from a given codon sequence
def codon_table(codon):

    c_table = {"UUU":"F", "UUC": "F", "UUA":"L", "UUG":"L", "UCU":"S", "UCC":"S",
    "UCA":"S", "UCG":"S", "UAU":"Y", "UAC":"Y", "UAA": 0, "UAG": 0,
    "UGU":"C", "UGC":"C", "UGA": 0, "UGG":"W", "CUU":"L", "CUC":"L",
    "CUA":"L", "CUG":"L", "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q", "CGU":"R", "CGC":"R",
    "CGA":"R", "CGG":"R", "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T", "AAU":"N", "AAC":"N",
    "AAA":"K", "AAG":"K", "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V", "GCU":"A", "GCC":"A",
    "GCA":"A", "GCG":"A", "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}
    return c_table[codon]

# convert the RNA string to amino acid string
def to_amino_acid(s):
    codons =[]
    peptide_str = ''
    for i in range(0, len(s), 3):
        codons.append(s[i:i+3])
    stopped = False
    for codon in codons:
        acid = codon_table(codon)
        if acid == 0:  
           break
        else:
            peptide_str += str(acid)
    return peptide_str

# exchange all instances of 'T' with 'U'
def to_rna(T):
    rna = ''
    for i in range(len(T)):
        char = T[i]
        if char == 'T':
            rna += 'U'
        else:
            rna += char
    return rna
    
T = ''
introns = []

hasT = False
line = sys.stdin.readline()
while (True):
    if len(line) == 0:
        break
    elif line[0] == '>' and hasT == False:
        line = sys.stdin.readline().strip()
        hasT = True
        while line[0] != '>':
            T = T + line
            line = sys.stdin.readline().strip()
    elif line[0] == '>' and hasT == True:
        introns.append(sys.stdin.readline().strip())
        line = sys.stdin.readline()
index_hits = {}
for intron in introns:
    # for each intron create an inverted index
    # of substrings length |intron|. Then query and
    # find all hits. Put into index_hits map
    # key 'hit' (index) and value intron
    inverted_index = IndexHash.IndexHash(T,len(intron),1)
    hits = inverted_index.query(intron)
    for hit in hits:
        index_hits[hit] = intron

# order the dictionary
index_hits = collections.OrderedDict(sorted(index_hits.items()))

# filter the exons by noting the hit index and
# length of the exon
new_T = ''
initial = 0
for key in index_hits.keys():
    length = len(index_hits[key])
    end = key
    #print end, index_hits[key]
    new_T += T[initial:end]
    initial = end + length
new_T += T[initial:]
peptide = to_amino_acid(to_rna(new_T))
print peptide
