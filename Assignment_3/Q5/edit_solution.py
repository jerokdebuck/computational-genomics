#!/usr/bin/python

import sys
import numpy as np


sys.stdin.readline()
s = ''
while(True):
    line = sys.stdin.readline().strip()
    if line[0] == '>': break
    s += line

t = ''
line = sys.stdin.readline().strip()
while len(line) != 0:
    t += line
    line = sys.stdin.readline().strip()

# Dynamic programming method
D = np.zeros((len(s) + 1, len(t) + 1), dtype=int)
D[0, 1:] = range(1, len(t) + 1)
D[1:, 0] = range(1, len(s) + 1)

for i in xrange(1, len(s) + 1):
    for j in xrange(1, len(t) + 1):
        delt = 1 if s[i-1] != t[j-1] else 0
        min_val = min(D[i-1,j-1]+delt, D[i-1,j]+1,D[i,j-1]+1)
        D[i,j] = min_val
# optimal edit distance in position D[-1, -1]
print D[-1,-1]

    
