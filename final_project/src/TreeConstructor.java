/** Allows users to create a TreeConsturctor object
*/
public interface TreeConstructor {

    /** Builds a tree based on distance inference.
        @return boolean if building was successful
    */
    public boolean build_tree();
    /** Print the nodes of a tree via BFS.
    */
    public void print_tree();
    /** Print the hashmap coding for the distance matrix
    */
    public void print_dmatrix();
}
