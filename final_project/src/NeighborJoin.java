import java.util.*;

/** Neighbor Join algorithm. Implements TreeConstructor
*/
public class NeighborJoin implements TreeConstructor {

    // Node inner class reperesenting the nodes of the tree
    private class Node {
        public String name;
        public HashSet<Node> children;
        public Node parent;
        public double edgelength;

        /** Creates a Node instance.
            @param n the label of the node
        */
        public Node(String n) {
            name = n;
            children = new HashSet<Node>();
            parent = this;
            edgelength = -1.0;
        }
        /** Checks to see if the label of Nodes are equal.
            @param o the Node object
            @return boolean true if labels equal false otherwise
        */
        public boolean equals(Object o) {
            if (!(o instanceof Node)) {
                return false;
            }
            Node n = (Node) o;
                System.out.println(this.name);
                System.out.println(n.name);
            if (this.name.equals(n.name)) {
                return true;
            }
            return false;
        }
    }
    private HashMap<String[], Double> distanceMatrix;
    private Node root;
    private int jointNumber;

    /** Creates NeighborJoin instance
        @param dm the distance matrix encoded a a hashmap
    */
    public NeighborJoin(HashMap<String[], Double> dm) {
        distanceMatrix = dm;
        root = new Node("ROOT");
        jointNumber = 1;
    }
    /** Builds the Neighbor Join tree.
        @return boolean if the tree is built successfully
    */
    public boolean build_tree() {
        initializeRoot(activeNodes());
        while (activeNodes().size() > 2) {
            HashMap<String, Double> sValues = calculateS();
            String[] pair = calculateMinDSDiff(sValues);
            String joint = joinNeighbors(pair, sValues);
            recalculateDistanceMatrix(joint, pair);
        }
        connectLastNodes();
        return true;
    }
    private HashSet<String> activeNodes() { //get labels from distance matrix
        HashSet<String> names = new HashSet<String>();
        for (String[] s : distanceMatrix.keySet()) {
            for (int i = 0; i < 2; i++) {
                names.add(s[i]);
            }
        }
        return names;
    }
    private void initializeRoot(HashSet<String> names) {
    //build ambiguous tree with central node as starting point of neighbor join algorithm
        for (String s : names) {
            Node n = new Node(s);
            n.parent = root;
            root.children.add(n);
        }
    }
    // calculates all the S values where S = sum(D_ix)/(N-2)
    private HashMap<String, Double> calculateS() {
        HashMap<String, Double> sValues = new HashMap<String, Double>();
        for (String an : activeNodes()) {
            double sum = 0.0;
            for (String[] pair : distanceMatrix.keySet()) {
                if (pair[0].equals(an) || pair[1].equals(an)) {
                    sum += distanceMatrix.get(pair);
                }
            }
            double s = sum / (double) (activeNodes().size() - 2);
            sValues.put(an, s);
        }
        return sValues;
    }
    // Using the S values, calculates the min D_ij - S_i - S_j
    private String[] calculateMinDSDiff(HashMap<String, Double> sValues) {
        String[] join = new String[2];
        double min = Double.MAX_VALUE;
        double next;
        for (String[] pair : distanceMatrix.keySet()) {
            next = distanceMatrix.get(pair) - sValues.get(pair[0]) - sValues.get(pair[1]);
            if (next < min) {
                min = next;
                join = pair;
            }
        }
        return join;
    }
    // logic for joining neighbors
    private String joinNeighbors(String[] pair, HashMap<String, Double> sValues) {
        double e1 = (distanceMatrix.get(pair) / 2.0) + ((sValues.get(pair[0]) - sValues.get(pair[1])) / 2.0);
        double e2 = (distanceMatrix.get(pair) / 2.0) + ((sValues.get(pair[1]) - sValues.get(pair[0])) / 2.0);
        String jointName = "u" + jointNumber;
        jointNumber++;
        Node joint = new Node(jointName);
        joint.parent = root;
        root.children.add(joint);
        Iterator<Node> iter = root.children.iterator();
        Node n;
        while (iter.hasNext()) {
            n = iter.next();
            if (n.name.equals(pair[0])) {
                n.parent = joint;
                joint.children.add(n);
                iter.remove();
                n.edgelength = e1;
            } else if (n.name.equals(pair[1])) {
                n.parent = joint;
                joint.children.add(n);
                iter.remove();
                n.edgelength = e2;
            }
        }
        return joint.name;
    }
    // recalculates the distance matrix
    private void recalculateDistanceMatrix(String newName, String[] oldNames) {
        for (String an : activeNodes()) {
            if (!oldNames[0].equals(an) && !oldNames[1].equals(an)) {
                double d1 = getDistance(an, oldNames[0]);
                double d2 = getDistance(an, oldNames[1]);
                double d3 = distanceMatrix.get(oldNames);
                double newDistance = (d1 + d2 - d3) / 2.0;
                String[] newPair = {newName, an};
                distanceMatrix.put(newPair, newDistance);
            }
        }
        Iterator<String[]> iter = distanceMatrix.keySet().iterator();
        String[] pair;
        while (iter.hasNext()) {
            pair = iter.next();
            if (pair[0].equals(oldNames[0])) {
                iter.remove();
            } else if (pair[1].equals(oldNames[0])) {
                iter.remove();
            } else if (pair[0].equals(oldNames[1])) {
                iter.remove();
            } else if (pair[1].equals(oldNames[1])) {
                iter.remove();
            }
        }
    }
    private double getDistance(String s1, String s2) { //find pair when you don't know the order
        for (String[] s : distanceMatrix.keySet()) {
            if (s[0].equals(s1) && s[1].equals(s2)) {
                return distanceMatrix.get(s);
            } else if (s[0].equals(s2) && s[1].equals(s1)) {
                return distanceMatrix.get(s);
            }
        }
        System.err.println("cant find pair " + s1 + " " + s2);
        return -1.0;
    }
    private void connectLastNodes() {
        Iterator<Node> iter = root.children.iterator();
        //arbitrary root designation
        Node node1 = iter.next();
        Node node2 = iter.next();
        root = node1;
        root.parent = node1;
        root.children.add(node2);
        node2.parent = node1;
        node2.edgelength = getDistance(node1.name, node2.name);
    }
    /** Prints the tree using BFS. Output in a way graphwiz
        can use to turn into a picture
    */
    public void print_tree() {
        System.out.println("graph NeighborJoin {");
        Queue<Node> q = new LinkedList<Node>();
        q.add(root);
        while (q.peek() != null ) {
            Node n = q.poll();
            for (Node child : n.children) {
                if (child != null) {
                    System.out.printf("    " + n.name + "--" + child.name + " [label=%.3f];\n", child.edgelength);
                    q.add(child);
                }
            }
        }
        System.out.println("}");
    }
    public void printsd(HashMap<String, Double> h) {
        for (String s : h.keySet()) {
            System.out.println(s + "\t" + h.get(s));
        }
    }
    public void printsad(HashMap<String[], Double> h) { //assumes String[] is length 2
        for (String[] s : h.keySet()) {
            System.out.println(s[0] + "," + s[1] + "\t" + h.get(s));
        }
    }
    /** Prints the distance matrix.
    */
    public void print_dmatrix() {
        printsad(distanceMatrix);
    }
   /*
    public static void main(String[] args) { //test!
        HashMap<String[], Double> testmap = new HashMap<String[], Double>();

        String[] pair1 = {"A", "B"};
        double distance1 = 5.0;
        testmap.put(pair1, distance1);
     
        String[] pair2 = {"C","B"};
        double distance2 = 7.0;
        testmap.put(pair2, distance2);
 
        String[] pair3 = {"C","A"};
        double distance3 = 4.0;
        testmap.put(pair3, distance3);
 
        String[] pair4 = {"D","B"};
        double distance4 = 10.0;
        testmap.put(pair4, distance4);
 
        String[] pair5 = {"D", "A"};
        double distance5 = 7.0;
        testmap.put(pair5, distance5);
 
        String[] pair6 = {"E", "B"};
        double distance6 = 9.0;
        testmap.put(pair6, distance6);
 
        String[] pair7 = {"E", "A"};
        double distance7 = 6.0;
        testmap.put(pair7, distance7);
 
        String[] pair8 = {"E","C"};
        double distance8 = 6.0;
        testmap.put(pair8, distance8);

        String[] pair9 = {"E","D"};
        double distance9 = 5.0;
        testmap.put(pair9, distance9);
 
        String[] pair10 = {"D","C"};
        double distance10 = 7.0;
        testmap.put(pair10, distance10);

        String[] pair11 = {"F","A"};
        double distance11 = 8.0;
        testmap.put(pair11, distance11);

        String[] pair12 = {"F","B"};
        double distance12 = 11.0;
        testmap.put(pair12, distance12);

        String[] pair13 = {"F","C"};
        double distance13 = 8.0;
        testmap.put(pair13, distance13);

        String[] pair14 = {"F","D"};
        double distance14 = 9.0;
        testmap.put(pair14, distance14);

        String[] pair15 = {"F","E"};
        double distance15 = 8.0;
        testmap.put(pair15, distance15);

        TreeConstructor test = new NeighborJoin(testmap);
        test.build_tree();
        test.print_tree();
    } */
}
