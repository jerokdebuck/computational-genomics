import java.util.*;

/** implementation of an algorithm for multiple sequence alignment.
    constructor takes a HashMap as an argument
    as of now, the constructor prints prints a progress bar to standard err
    alignments fills one half of a 2 dimensional array which has indexes which
    correspond to a second one dimensional array which stores just the names of
    our sequences
    parsing input to hashmap is implemented in Phylo.java
*/
public class DistanceMatrix {
    private HashMap<String, String> sequences;
    private String[] names;
    private Double[][] alignments;

    /** Creates DistanceMatrix instance.
        @param seq the hashmap representation of the distance matrix
    */
    public DistanceMatrix(HashMap<String, String> seq) {
        sequences = seq;
        initializeDataStructures();
        //initialize the variables used to print the progress bar
        int totalAlignments = numAlignments(sequences.size());
        int finishedAlignments = 0;
        int totalDashes = 56;
        int printedDashes = 0;
        System.err.println("Aligning Sequences:");
        System.err.println("[0%            25%           50%           75%           ]");
        System.err.print("[");
        //align each pair of sequences
        for (int i = 0; i < sequences.size(); i++) {
            for (int j = 0; j < sequences.size(); j++) {
                if (i > j) {
                    alignments[i][j] = align(sequences.get(names[i]), sequences.get(names[j]));
                    //print progress bar
                    finishedAlignments++;
                    while (((double) finishedAlignments / (double) totalAlignments)
                        > ((double) printedDashes / (double) totalDashes)) {
                        System.err.print("-");
                        printedDashes++;
                    }
                }
            }
        }
        System.err.println("]");
    }
    /** Naive dynamic programming algorithm to calculate the edit distance
        @param seq1 the first string
        @param seq2 the second string
        @return double indicating the edit distance
    */
    public double align(String seq1, String seq2) {
        int width = seq1.length() + 1;
        int height = seq2.length() + 1;
        Double[][] matrix = new Double[width][height];
        for (int i = 0; i < width; i++) {
            matrix[i][0] = (double) i;
        }
        for (int i = 1; i < height; i++) {
            matrix[0][i] = (double) i;
        }
        double delt;
        for (int i = 1; i < height; i++) {
            for (int j = 1; j < width; j++) {
                if (seq2.charAt(i-1) != seq1.charAt(j-1)) {
                    delt = 1.0;
                } else {
                    delt = 0.0;
                }
                matrix[j][i] = min(matrix[j-1][i-1]+delt, matrix[j-1][i]+1, matrix[j][i-1]+1);
            }
        }
        //print(seq1, seq2, matrix);
        return matrix[width-1][height-1];
    }
    /** Finds the minimum value from a triplet of doubles.
        @param a the first double
        @param b the second double
        @param c the third double
        @return double indicated the min of a,b,c
    */
    public double min(double a, double b, double c) {
        double min = a;
        if (min > b) { min = b; }
        if (min > c) { min = c; }
        return min;
    }
    /** Print a truncated matrix for testing
        @param seq1 the first sequence
        @param seq2 the second sequence
        @param matrix the distance matrix
    */
    public void print(String seq1, String seq2, Double[][] matrix) { //print a truncated matrix
        //works great for tracing algorithm for sequences of ~40 or less
        //any longer and the output is illegible
        int width = seq1.length() + 1;
        int height = seq2.length() + 1;
        System.err.println();
        System.err.print("       ");
        for (int i = 1; i < width; i++) {
            System.err.print(seq1.charAt(i-1) + "  ");
        }
        System.err.println();
        for (int i = 0; i < height; i++) {
            if (i != 0) {
                System.err.print(seq2.charAt(i-1) + "  ");
            } else {
                System.err.print("   ");
            }
            for (int j = 0; j < width; j++) {
                System.err.printf("%2.0f ", matrix[j][i]);
            }
            System.err.println();
        }
    }
    /** allows driver to construct a DistanceMatrix, then call method to return underlying matrix. This needs to return a map from sequence name pairs to edit distance
        @return HashMap the hashmap data structure coding for the distance matrix
    */
    public HashMap<String[], Double> getAlignments() {
        HashMap<String[], Double> dm = new HashMap<String[], Double>();
        for (int i = 0; i < sequences.size(); i++) {
            for (int j = 0; j < sequences.size(); j++) {
                if (alignments[i][j] != 1000.0) {
                    String[] pair = {names[i], names[j]};
                    dm.put(pair, alignments[i][j]);
                }
            }
        }
        return dm;
    }
    /** Like get alignments but uses Jukes-Cantor metric to obtain the
        corrected distance matrix reperesented by a hashmap.
        @return Hashmap the data structure endocing the distance matrix
    */
    public HashMap<String[], Double> getPercentDifferenceAlignments() {
        HashMap<String[], Double> dm = new HashMap<String[], Double>();
        for (int i = 0; i < sequences.size(); i++) {
            for (int j = 0; j < sequences.size(); j++) {
                if (alignments[i][j] != 1000.0) {
                    String seq1 = sequences.get(names[i]);
                    String seq2 = sequences.get(names[j]);
                    double distance = alignments[i][j];
                    double pd1 = ( distance / seq1.length() );
                    double pd2 = ( distance / seq2.length() );
                    double percent_difference = (pd1+pd2) / 2.0;
                    String[] pair = {names[i], names[j]};
                    double new_distance = -.75*java.lang.Math.log(1-((4/3)*percent_difference));
                    double temp = new_distance*1000;
                    temp = java.lang.Math.round(temp);
                    new_distance = temp / 1000;
                    dm.put(pair, new_distance);
                }
            }
        }
        return dm;
    }
    private void initializeDataStructures() {
        //initialize the two data structures (other than the hash map)
        //that this algorithm will use
        names = new String[sequences.size()];
        alignments = new Double[sequences.size()][sequences.size()];
        int pointer = 0;
        for (String s : sequences.keySet()) {
            names[pointer] = s;
            for (int i = 0; i <sequences.size(); i++) {
                alignments[pointer][i] = 1000.0;
            }
            pointer++;
        }
    }
    /** Acquires the total number of unique alignments.
        @param n the number of OTUs
        @return int the number of unique alignments
    */
    public int numAlignments(int n) { //number of unique pairs of n sequences
        int sum = 0;
        for (n -= 1; n > 0; n--) {
            sum += n;
        }
        return sum;
    }
    /** Prints the alignments.
    */
    public void printAlignments() {
        for (int i = 0; i < sequences.size(); i++) {
            for (int j = 0; j < sequences.size(); j++) {
                if (alignments[i][j] != 1000.0) {
                    System.out.println("[" + names[i] + ", " + names[j] + "]");
                    System.out.println("\t" + alignments[i][j]);
                }
            }
        }
    }
}
