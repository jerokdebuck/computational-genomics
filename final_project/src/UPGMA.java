import java.util.HashMap;
import java.util.Queue;
import java.util.ArrayList;
import java.util.LinkedList;

/** Distance based inference using UPGMA algorithm. 
    Implements TreeConstructor.
*/
public class UPGMA implements TreeConstructor {

    // An inner class representing a node of the tree
    private class Node {
        private int init_children = 4;
        private int num_children;
        private Node[] children;
        private String label;
        private double distance;
        // not necessary in this implementation but in case the number of 
        // children exceeds the data structure encoding the children size
        private void double_size() {
            int new_size = this.num_children * 2;
            Node[] new_children = new Node[new_size];
            for (int i = 0; i < this.num_children; i++) {
                new_children[i] = this.children[i];
            }
            this.children = new_children;
        }
        // checks to see if the size of data structure coding for children
        // is equal to the number of children
        private boolean isFull() {
            return this.children.length == this.num_children;
        }
        /** Creates a Node instance
            @param label the OTU of the node
            @param distance the distance of the OTU to its parent
        */
        public Node(String label, double distance) {
            this.label = label;
            this.distance = distance;
            this.children = new Node[this.init_children];
        }
        /** Return the label of the node.
            @return String the label
        */
        public String return_label() {
            return this.label;
        }
        /** Returns the distance in the node.
            @return double the distance
        */
        public double return_distance() {
            return this.distance;
        }
        /** Changes the node's label.
            @param label the new label
        */
        public void change_label(String label) {
            this.label = label;
        }
        /** Changes the node's distance.
            @param distance the new distance
        */
        public void change_distance(double distance) { 
            this.distance = distance;
        }
        /** Returns the Node's children.
            @return Node[] the array containing the node's children
        */
        public Node[] return_children() {
            return this.children;
        }
        /** Returns the number of children.
            @return int the number of children
        */
        public int return_num_children() {
            return this.num_children;
        }
        /** Adds a child to the node.
            @param str the label of the child node
            @param val the distance value of the child node
        */
        public void add_child(String str, double val) {
            if (this.isFull()) {
                this.double_size();
            }
            this.children[this.num_children] = new Node(str, val);
            this.num_children += 1;
        }
        /** Adds a child to the node
            @param n the node child
        */
        public void add_child(Node n) {
            if (this.isFull()) {
                this.double_size();
            }
            this.children[this.num_children] = n;
            this.num_children += 1;
        }
           
    }
    // a minor class that holds key val pairs
    private class TempContainer {
        private String[] key;
        private double val;
        /** Creates a TempContainer instance.
            @param key the String[] key
            @param val the distance
        */
        public TempContainer(String[] key, double val) {
            this.key = key;
            this.val = val;
        }
        /** Returns the key.
            @return String[] the key
        */
        public String[] return_key() {
            return this.key;
        }
        /** Returns the value.
            @return double the value
        */
        public double return_value() {
            return this.val;
        }
    }

    private HashMap<String[], Double> dmatrix_map;
    private boolean init = false;
    private Node root;
    private HashMap<String, Node> subtrees;

    /** Creates a UPGMA instance.
        @param dmatrix_map the data structure coding for the distance matrix
    */
    public UPGMA(HashMap<String[], Double> dmatrix_map) {
        // make sure dmatrix_map is not null
        this.dmatrix_map = dmatrix_map;
        if (this.dmatrix_map != null) {
            this.init = true;
            this.root = new Node("ROOT", 0.0);
            this.subtrees = new HashMap<String, Node>();
                    
        }
    }

    // rebuilds the distance matrix based on iterative clustering
    private void rebuild_dmatrix(String k1, String k2) {
        // intermediateMatrix is an intermediate data structure containing 
        // (OTU, distance) metrix pairs. finalMatrix is the final version
        // of the updated distance matrix
        HashMap<String, Double> intermediateMatrix = new HashMap<String, Double>();
        HashMap<String[], Double> finalMatrix = new HashMap<String[], Double>();
        // iterrate through all keys
        for (String[] k: this.dmatrix_map.keySet()) {
            // check if the first OTU in the key (String[]) is k1 or k2
            // where k1 and k2 represent the OTUs selected to be collapsed
            if (k[0].compareTo(k1) == 0 || k[0].compareTo(k2) == 0) {
                double dist1 = this.dmatrix_map.get(k); 
                // check if the second OTU in String[] key is in the intermediate matrix
                String testKey = k[1];
                if (intermediateMatrix.containsKey(testKey)) {
                    // if it is, calculate final distance as .5(dist1+dist2)
                    double dist2 = intermediateMatrix.get(testKey);
                    double final_dist = (dist1 + dist2) / 2.0;
                    String[] final_key = {k1+k2, k[1]};
                    finalMatrix.put(final_key, final_dist);
                } else { 
                    // else put the OTU in the intermediate matrix
                    intermediateMatrix.put(testKey, dist1); 
                }
            }
            // performs the same operation
            else if (k[1].compareTo(k1) == 0|| k[1].compareTo(k2) == 0) {
                double dist1 = this.dmatrix_map.get(k);
                String testKey = k[0];
                if (intermediateMatrix.containsKey(testKey)) {
                    double dist2 = intermediateMatrix.get(testKey);
                    double final_dist = (dist1 + dist2) / 2.0;
                    String[] final_key = {k1+k2, k[0]};
                    finalMatrix.put(final_key, final_dist);
                } else {
                    intermediateMatrix.put(testKey, dist1);
                }
            } else {
                finalMatrix.put(k, this.dmatrix_map.get(k));
            }
        }
        // give the private variable the new map
        this.dmatrix_map = finalMatrix;
    }
    /** Builds the UPGMA tree.
        @return boolean if construction was successful
    */           
    public boolean build_tree() {
        // make sure the tree has been initialized correctly
        if (!this.init) {
            return false;
        }
        Node curr = this.root;
        String alt_label = "u"; // concatenated labeling
        int i = 0;
        while(true) {
           // For each iteration, acquire the minimum distance
           // from the distance matrix representation
           TempContainer min_instance = this.get_min_distance();
           // if fully collpased, building was successful
           if (min_instance == null) {
               return true;
           }
           String[] key = min_instance.return_key();
           String str1 = key[0];
           String str2 = key[1];
           // rebuild the matrix after acquiring min
           this.rebuild_dmatrix(str1, str2);
           double distance = min_instance.return_value();
           // if a subtree has been built containing one of the key OTUs
           if (this.subtrees.containsKey(str1) || this.subtrees.containsKey(str2) ) {
               Node subtree_root = new Node(alt_label+String.valueOf(i), 0.0);
               i += 1;
               // if str1 has been built prior, acquire that tree
               // child of root will be the subtree labeled by str1
               if (this.subtrees.containsKey(str1)) {
                   Node prev_subtree = this.subtrees.get(str1);
                   subtree_root.change_distance(distance/2.0);
                   prev_subtree.change_distance(distance/2.0 - prev_subtree.return_distance());
                   subtree_root.add_child(prev_subtree);
                   // check if str2 has been built in subtrees, if not then
                   // add to root's child a node containign the OTU str2
                   if (!this.subtrees.containsKey(str2)) {
                       subtree_root.add_child(str2, distance / 2.0);
                   }
               } 
               // same idea except for str2
               if (this.subtrees.containsKey(str2)) { 
                   Node prev_subtree = this.subtrees.get(str2);
                   subtree_root.change_distance(distance/2.0);
                   prev_subtree.change_distance(distance/2.0 - prev_subtree.return_distance());
                   subtree_root.add_child(prev_subtree);
                   if (!this.subtrees.containsKey(str1)) {
                       subtree_root.add_child(str1, distance / 2.0);
                   }
               }
               this.subtrees.put(str1+str2, subtree_root);
               this.root = subtree_root;

           } else {
               // else create a subtree with root each to the concateneated label
               // at iteration 1 nad each branch length each to distance / 2
               // The two children nodes will represent the OTU from the minimum
               // key chosen
               Node subtree_root = new Node(alt_label+String.valueOf(i), distance/2.0);
               i += 1;
               subtree_root.add_child(str1, distance / 2.0);
               subtree_root.add_child(str2, distance / 2.0);
               this.subtrees.put(str1+str2, subtree_root);
               //this.root = subtree_root;
           }    
               
        }
    }
    /** Prints the tree using BFS. Outputs in such a way
        that graphwiz can turn into a visual representation of the tree
    */
    public void print_tree() {
        System.out.println("digraph UPGMA {");
        Queue<Node> q = new LinkedList<Node>();
        q.add(this.root);
        while (q.peek() != null ) {
            Node n = q.poll();
            for (Node child : n.return_children() ) {
                if (child != null) {
                    System.out.printf("    " + n.return_label() + "->" + child.return_label() + " [label=%.3f];\n", child.return_distance() );
                    q.add(child);
                }
            }
        }
        System.out.println("}");
    }
    /** Prints the distance matrix representation
    */
    public void print_dmatrix() {
        for (String[] k : this.dmatrix_map.keySet()) {
            System.out.println(k[0] + " " + k[1] + " " + this.dmatrix_map.get(k) );
        }
    }        
        
    // acquires the min distance from distance matrix. Outputs
    // a TempContainer instance containing min keys and min distance
    private TempContainer get_min_distance() {
        if (this.dmatrix_map.isEmpty() ) {
            return null;
        }
        double min_distance = 1000.0;
        String[] min_keys = new String[2];
        for (String[] key : this.dmatrix_map.keySet() ) {
            double dist = this.dmatrix_map.get(key);
            if (dist <= min_distance) {
                min_distance = dist;
                min_keys = key;
            }
        }
        this.dmatrix_map.remove(min_keys);
        return new TempContainer(min_keys, min_distance);
    }
        
            
    // test main method, not used in final output
    /*
    public static void main(String[] args) {
        HashMap<String[], Double> testmap = new HashMap<String[], Double>();

        String[] animal1 = {"A", "B"};
        double distance1 = 2.0;
        testmap.put(animal1, distance1);

        String[] animal2 = {"A","C"};
        double distance2 = 4.0;
        testmap.put(animal2, distance2);
 
        String[] animal3 = {"A","D"};
        double distance3 = 6.0;
        testmap.put(animal3, distance3);
 
        String[] animal4 = {"A","E"};
        double distance4 = 6.0;
        testmap.put(animal4, distance4);
 
        String[] animal5 = {"A","F"};
        double distance5 = 8.0;
        testmap.put(animal5, distance5);

        String[] animal6 = {"B","C"};
        double distance6 = 4.0;
        testmap.put(animal6, distance6);

        String[] animal7 = {"B", "D"};
        double distance7 = 6.0;
        testmap.put(animal7, distance7);
 
        String[] animal8 = {"B","E"};
        double distance8 = 6.0;
        testmap.put(animal8, distance8);
 
        String[] animal9 = {"B","F"};
        double distance9 = 8.0;
        testmap.put(animal9, distance9);

        String[] animal10 = {"C", "D"};
        double distance10 = 6.0;
        testmap.put(animal10, distance10);
 
        String[] animal11 = {"C", "E"};
        double distance11 = 6.0;
        testmap.put(animal11, distance11);

        String[] animal12 = {"C", "F"};
        double distance12 = 8.0;
        testmap.put(animal12, distance12);
 
        String[] animal13 = {"D", "E"};
        double distance13 = 4.0;
        testmap.put(animal13, distance13);
 
        String[] animal14 = {"D", "F"};
        double distance14 = 8.0;
        testmap.put(animal14, distance14);

        String[] animal15 = {"E", "F"};
        double distance15 = 8.0;
        testmap.put(animal15, distance15);

        String[] animal1 = {"A", "B"};
        double distance1 = 53.0;
        testmap.put(animal1, distance1);
     
        String[] animal2 = {"C","B"};
        double distance2 = 52.0;
        testmap.put(animal2, distance2);
 
        String[] animal3 = {"C","A"};
        double distance3 = 44.0;
        testmap.put(animal3, distance3);
 
        String[] animal4 = {"D","B"};
        double distance4 = 52.0;
        testmap.put(animal4, distance4);
 
        String[] animal5 = {"D", "A"};
        double distance5 = 44.0;
        testmap.put(animal5, distance5);
 
        String[] animal6 = {"E", "B"};
        double distance6 = 57.0;
        testmap.put(animal6, distance6);
 
        String[] animal7 = {"E", "A"};
        double distance7 = 49.0;
        testmap.put(animal7, distance7);
 
        String[] animal8 = {"E","C"};
        double distance8 = 26.0;
        testmap.put(animal8, distance8);

        String[] animal9 = {"E","D"};
        double distance9 = 26.0;
        testmap.put(animal9, distance9);
 
        String[] animal10 = {"D","C"};
        double distance10 = 0.0;
        testmap.put(animal10, distance10);

        TreeConstructor test = new UPGMA(testmap);
        test.build_tree();
        test.print_tree();
    } */
}
    
