import java.io.*;
import java.util.*;

/** Driver class for 600.439 Computational Genomics final project.
    This class provides tools for users to decide on the tree and metric
    of interest. In particular, UPGMA and Neighbor Join implementations
    can be called on, in either their corrected (Jukes-Cantor) and non
    corrected versions
*/
public class Phylo {
    private Phylo() {}
    private static HashSet<String> argumentTable; //list of appropriate arguments

    /** main for Phylo.
        @param args the input command line arguments
    */
    public static void main(String[] args) {
        constructArgumentTable();
        //ensure proper input
        if (args.length < 2) {
            printHelp();
            return;
        }
        //input is proper length
        if (!argumentTable.contains(args[0])) {
            printHelp();
            return;
        }
        //argument specifies proper tree constructor
        Scanner sc;
        try {
            sc = new Scanner(new FileReader(args[1]));
        } catch (FileNotFoundException e) {
            System.err.println("Sequence file " + args[1] + " not found");
            return;
        }
        //argument specifies proper input file (or at least input file that exists)
        HashMap<String, String> sequences = parseFastA(sc); //map from sequence name to sequence
        //use sequences, DistanceMatrix, and constructor to make our tree
        DistanceMatrix dm = new DistanceMatrix(sequences);
        //dm.printAlignments();
        //a ho-hum switch to build the proper TreeConstructor
        TreeConstructor constructor = null;
        switch (args[0]) {
            case "u":
                constructor = new UPGMA(dm.getAlignments());
                break;
            case "n":
                constructor = new NeighborJoin(dm.getAlignments());
                break;
            case "cu":
                constructor = new UPGMA(dm.getPercentDifferenceAlignments() );
                break;
            case "cn":
                constructor = new NeighborJoin(dm.getPercentDifferenceAlignments() );
                break;
        }
        //build the tree
        //constructor.print_dmatrix();
        constructor.build_tree();
        //and what did we get?
        constructor.print_tree();
    }
    private static void constructArgumentTable() { //builds a map from arguments to tree constructor
        argumentTable = new HashSet<String>();
        argumentTable.add("u");
        argumentTable.add("cu");
        argumentTable.add("n");
        argumentTable.add("cn");
    }
    public static void printHelp() { //prints help when the user needs it
        System.err.println("Usage: java Phylo [constructor type] [sequence file]");
        System.err.println("Constructor type options:");
        System.err.println("\tu\tUPGMA");
        System.err.println("\tcu\tCorrected UPGMA");
        System.err.println("\tn\tNeighbor Join");
        System.err.println("\tcn\tCorrected Neighbor Join");
    }
    private static HashMap<String, String> parseFastA(Scanner sc) { //read a fasta file into a hash map
        HashMap<String, String> sequences = new HashMap<String, String>();
        String name = "";
        String sequence = "";
        String line;
        while (sc.hasNext()) {
            line = sc.nextLine();
            if (line.charAt(0) == '>') {
                if (!sequence.equals("") && !name.equals("")) {
                    sequences.put(name, sequence);
                }
                name = line.substring(1);
                sequence = "";
            } else {
                sequence += line;
            }
        }
        if (!sequence.equals("") && !name.equals("")) {
            sequences.put(name, sequence);
        }
        return sequences;
    }
}
