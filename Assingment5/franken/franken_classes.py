# A simple class to parse both types of files
class Parser(object):

    def __init__(self):
        self.init = True

    def parse_fasta(self,f):
        lines = []
        fi = open(f, "r")
        fi.readline()
        while True:
            l = fi.readline()
            if len(l) == 0: break
            l = l.strip()
            lines.append(l)
        fi.close()
        return ''.join(lines)
            

    def parse_non_fasta(self,f):
        fi = open(f, "r")
        line = fi.readline().strip()
        fi.close()
        return line

import numpy
# A class that builds emission and transition matrix
class Trainer(object):
    def __init__(self, train_genome, train_data):
        assert len(train_genome) == len(train_data)
        self.train_pairwise = []
        for i in xrange(0, len(train_genome)):
            self.train_pairwise.append( (train_genome[i], train_data[i]) )

    def train(self, states, alphabet):
        # Create a simple mapping from nucleotide -> integer
        # Useful for indexing matrices
        self.alphabet_map = {}
        for i in xrange(0, len(alphabet)):
            self.alphabet_map[alphabet[i]] = i

        self.state_map = {}
        for i in xrange(0, len(states)):
            self.state_map[str(states[i])] = i
        # Create len(states) x len(alphabet) emission matrix
        # Containing the probabilities of having a particular alphabet
        # in a state
        self.emission = numpy.zeros( (len(states), len(alphabet)), dtype=float)
        # find total number of instances
        for (alph, state) in self.train_pairwise:
            self.emission[self.state_map[state], self.alphabet_map[alph]] += 1
        i = 0
        # find probabilities by summing across the rows and diving by sum
        for row in self.emission:
            total = 0.0
            for r in row: total += r
            j = 0
            for r in row:
                self.emission[i,j] = float(r/total)
                j += 1
            i += 1
        
        self.init = {}
        for s in states:
            self.init[str(s)] = 0.0
        total = 0
        for i in xrange(0, len(self.train_pairwise)):
            (a,st) = self.train_pairwise[i]
            s_id = None
            for s in states:
                if int(st) == s:
                    s_id = s
            self.init[str(s_id)] += 1
            total += 1
        for k, v in self.init.iteritems():
            self.init[k] = float(self.init[k] / total)


        self.transition = numpy.zeros( (len(states),len(states)), dtype=float)
        for i in xrange(0, len(self.train_pairwise) -1):
            (a1,s1) = self.train_pairwise[i]
            (a2,s2) = self.train_pairwise[i+1]
            state1_id, state2_id = None, None
            for s in states:
                if int(s1) == s:
                    state1_id = str(s)
                if int(s2) == s:
                    state2_id = str(s)
            self.transition[self.state_map[state1_id], self.state_map[state2_id]] += 1
        i = 0
        for row in self.transition:
            total = 0.0
            for r in row: total += r
            j = 0
            for r in row:
                self.transition[i,j] = float(r/total)
                j += 1
            i += 1

        return self.alphabet_map, self.state_map, self.emission, self.transition, self.init

# Class provided by Prof Langmead
class HMM(object):
    def __init__(self, transition, emission, init):
        self.Q, self.S = set(), set()
        for a, prob in transition.iteritems():
            asrc, adst = a[0], a[1]
            self.Q.add(asrc)
            self.Q.add(adst)

        for e, prob in emission.iteritems():
            eq, es = e[0], e[1]
            self.Q.add(eq)
            self.S.add(es)
        self.Q = sorted(list(self.Q))
        self.S = sorted(list(self.S))

 
        qmap, smap = {}, {}
        for i in xrange(len(self.Q)): qmap[self.Q[i]] = i
        for i in xrange(len(self.S)): smap[self.S[i]] = i
        lenq = len(self.Q)

        self.A = numpy.zeros(shape=(lenq,lenq),dtype=float)
        for a, prob in transition.iteritems():
            asrc, adst = a[0], a[1]
            self.A[qmap[asrc], qmap[adst]] = prob
        self.A /= self.A.sum(axis=1)[:,numpy.newaxis]
        
        self.E = numpy.zeros(shape=(lenq, len(self.S)), dtype=float)
        for e, prob in emission.iteritems():
            eq, es = e[0], e[1]
            self.E[qmap[eq], smap[es]] = prob
        self.E /= self.E.sum(axis=1)[:,numpy.newaxis]

        self.I = [0.0] * lenq
        for a, prob in init.iteritems():
            self.I[qmap[a]] = prob
        self.I = numpy.divide(self.I, sum(self.I))
       
        self.qmap, self.smap = qmap, smap
        self.Alog = numpy.log2(self.A)
        self.Elog = numpy.log2(self.E)
        self.Ilog = numpy.log2(self.I)


    def jointProb(self, p, x):
        p = map(self.qmap.get, p)
        x = map(self.smap.get, x)
        tot = self.I[p[0]]
        for i in xrange(1, len(p)):
            tot *= self.A[p[i-1],p[i]]
        for i in xrange(0, len(p)):
            tot *= self.E[p[i],x[i]]
        return tot

    def jointProbL(self, p, x):
        p = map(self.qmap.get, p)
        x = map(self.smap.get, x)
        tot = self.Ilog[p[0]]
        for i in xrange(1, len(p)):
            tot += self.Alog[p[i-1], p[i]]
        for i in xrange(0, len(p)):
            tot += self.Elog[p[i], p[i]]
        return tot

    def viterbi(self, x):
        x = map(self.smap.get, x)
        nrow, ncol = len(self.Q), len(x)
        mat = numpy.zeros(shape=(nrow, ncol), dtype=float)
        matTb = numpy.zeros(shape=(nrow, ncol), dtype=int)
        
        for i in xrange(0, nrow):
            mat[i, 0] = self.E[i, x[0]] * self.I[i]
        for j in xrange(1, ncol):
            for i in xrange(0, nrow):
                ep = self.E[i, x[j]]
                mx, mxi = mat[0, j-1] * self.A[0,i] * ep, 0
                for i2 in xrange(1, nrow):
                    pr = mat[i2, j-1] * self.A[i2, i] * ep
                    if pr > mx:
                        mx, mxi = pr, i2
                mat[i,j], matTb[i,j] = mx, mxi

        omx, omxi = mat[0, ncol-1], 0
        for i in xrange(1, nrow):
            if mat[i, ncol-1] > omx:
                omx, omxi = mat[i,ncol-1], i
 
        i, p = omxi, [omxi]
        for j in xrange(ncol-1,0,-1):
            i = matTb[i,j]
            p.append(i)
        p = ''.join(map(lambda x: self.Q[x], p[::-1]))
        return omx, p

    def viterbiL(self, x):
        x = map(self.smap.get, x)
        nrow, ncol = len(self.Q), len(x)
        mat = numpy.zeros(shape=(nrow, ncol), dtype=float)
        matTb = numpy.zeros(shape=(nrow, ncol), dtype=int)
        for i in xrange(0, nrow):
            mat[i,0] = self.Elog[i, x[0]] + self.Ilog[i]
        for j in xrange(1, ncol):
            for i in xrange(0, nrow):
                ep = self.Elog[i, x[j]]
                mx, mxi = mat[0, j-1] + self.Alog[0,i] + ep, 0
                for i2 in xrange(1, nrow):
                    pr = mat[i2,j-1] + self.Alog[i2, i] + ep
                    if pr > mx:
                        mx, mxi = pr, i2
                mat[i,j], matTb[i,j] = mx, mxi
        omx, omxi = mat[0, ncol-1], 0
        for i in xrange(1, nrow):
            if mat[i, ncol-1] > omx:
                omx, omxi = mat[i, ncol-1], i
        i, p = omxi, [omxi]
        for j in xrange(ncol-1, 0, -1):
            i = matTb[i,j]
            p.append(i)
        p = ''.join(map(lambda x: self.Q[x], p[::-1]))
        return omx, p
