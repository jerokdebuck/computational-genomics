#!/usr/bin/python
import sys
import numpy
import franken_classes as classes

def reorganize_emission(emission, alphabet_map, states):
    fliped_alphabet = {}
    for k, v in alphabet_map.iteritems():
        fliped_alphabet[v] = k
    flipped_states = {}
    for k, v in states.iteritems():
        flipped_states[v] = k
    emission_map = {}
    for i in xrange(0, len(emission)):
        id1 = str(flipped_states[i])
        for j in xrange(0, len(emission[i])):
            id2 = fliped_alphabet[j]
            id_string = id1+id2
            emission_map[id_string] = emission[i,j]
    return emission_map

def reorganize_transition(transition, states):
    flipped_states = {}
    for k, v in states.iteritems():
        flipped_states[v] = k
    transition_map = {}
    for i in xrange(0, len(transition)):
       id1 = str(flipped_states[i])
       for j in xrange(0, len(transition[i])):
           id2 = str(flipped_states[j])
           id_string = id1+id2
           transition_map[id_string] = transition[i,j]
    return transition_map
           

genome_file = "input/frankengene1.fasta"
training_file = "input/trainingData1.txt"
test_file = "input/testData1.txt"

# Create Parser class and parse the genome, test, and training data
parse = classes.Parser()
genome_data = parse.parse_fasta(genome_file)
training_data = parse.parse_non_fasta(training_file)
test_data = parse.parse_non_fasta(test_file)

# Since we know the states and alphabet I will explicitly write
# them out rather than waste computation time figuring out the number
# of states and the size of the alphabet we are using
states = [0,1]
alphabet = ["A","T","G","C"]

# training_genome containins first 50000 nucleotides in frankengenome
training_genome = genome_data[0:50000]
# Create Classifier instance using training_genome and test_data
tr = classes.Trainer(training_genome, training_data)
# train to create emission and transition matrixes
alphabet_map, state_map, emission, transition, init = tr.train(states, alphabet)
# reogranize emission matrix
emission_map = reorganize_emission(emission, alphabet_map, state_map)
transition_map = reorganize_transition(transition, state_map)
# create HMM instance, which acts as the classifier class
classifier = classes.HMM(transition_map, emission_map, init)
test = genome_data[50000:100000]
# Perform a logarithmic viterbi on the test genome sequence
logprob, path = classifier.viterbiL(test)

# Find how accurate the solution is
total = 0.0
correct = 0
for i in xrange(0, len(path)):
    if path[i] == test_data[i]:
        correct += 1
    total += 1.0

output = open("output/output.txt", "w")
output.write('Accuracy: ')
output.write(str(100*(correct / total)))
output.write("%\n")
output.write(path)
output.close()
