#!/usr/bin/python

# Solution for Problem 5 of Assignment 5

import sys

# Acquire the reverse compliment of a DNA strand
def reverse_compliment(strand):
    
    # First reverse the string
    reverse = []
    for i in xrange(len(strand) -1,-1,-1):
        reverse.append(strand[i])
    # Then acquire the complementary nucleotides
    reverse_compliment = []
    for i in xrange(0, len(reverse)):
        reverse_compliment.append(comp(reverse[i]))
    return ''.join(reverse_compliment)

# Acquires complementary nucleotides
def comp(nuc):
    if nuc == "A":
        nuc = "T"
    elif nuc == "T":
        nuc = "A"
    elif nuc == "G":
        nuc = "C"
    else: nuc = "G"
    return nuc

# Acquire the three reading frames for a DNA sequence
# First frame is the entire sequence x1,x2,x3,...xn
# Second frame is x2,x3,...,xn-3,xn-2
# Third frame is x3,x4,...,xn-2,xn-1
def acquire_reading_frames(strand):
    frames = []
    frames.append(strand)
    frames.append(strand[1:-2])
    frames.append(strand[2:-1])
    return frames

# Map a codon to its letter using c_table
def codon_table(codon):
    # Make sure codon is length 3
    if len(codon) != 3: return
    rna_codon = []
    for c in codon:
        # change all T to U
        if c == "T":
            c = "U"
        rna_codon.append(c)
    rna_codon = ''.join(rna_codon)

    c_table = {"UUU":"F", "UUC": "F", "UUA":"L", "UUG":"L", "UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S", "UAU":"Y", "UAC":"Y", "UAA": "0", "UAG": "0", 
    "UGU":"C", "UGC":"C", "UGA": "0", "UGG":"W", "CUU":"L", "CUC":"L",
    "CUA":"L", "CUG":"L", "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q", "CGU":"R", "CGC":"R",
    "CGA":"R", "CGG":"R", "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T", "AAU":"N", "AAC":"N",
    "AAA":"K", "AAG":"K", "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R", 
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V", "GCU":"A", "GCC":"A",
    "GCA":"A", "GCG":"A", "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}
    return c_table[rna_codon]
# Convert a DNA strand to its amino acid sequence
def dna_to_amino(strand):
    # acquire codons
    codons = []
    for i in xrange(0, len(strand), 3):
        codons.append(strand[i:i+3])
    amino_acids = []
    # For each codon, gets its amino acid
    for codon in codons:
        acid = codon_table(codon)
        if acid != None:
            amino_acids.append(acid)
    return ''.join(amino_acids)

# For an amino acid sequence, extract those protein sequences starting with start codon and ending with stop codon
def protein_sequences(acid_sequence):
    proteins = []
    stop_codon = None
    # Loop through each amino acid
    for i in xrange(0, len(acid_sequence)):
        # Start codon acquired?
        if acid_sequence[i] == "M":
            curr_protein = ["M"]
            # Loop until a stop codon is hit
            while True:
                i += 1
                if i == len(acid_sequence):
                    stop_codon = False
                    break
                elif acid_sequence[i] == "0": 
                    stop_codon = True
                    break
                curr_protein.append(acid_sequence[i])
            if stop_codon == True:
                curr_protein = ''.join(curr_protein)
                proteins.append(curr_protein)
    return proteins
            
           
# Parse in data
header = sys.stdin.readline()
strand = []
while True:
    nucs = sys.stdin.readline()
    if len(nucs) == 0: 
        break
    nucs = nucs.strip()
    strand.append(nucs)

# Acquire the DNA sequence and its reverse compliment
strand = ''.join(strand)
strand_rev_comp = reverse_compliment(strand)

# Acquire the frames for DNA sequence and its reverse compliment
strand_frames = acquire_reading_frames(strand)
strand_rev_comp_frames = acquire_reading_frames(strand_rev_comp)

# Acquire proteins
protein_set = set()
for i in xrange(0, len(strand_frames)):
    # Acquire the amino acid seqeuence for the frame and its proteins
    amino_sequence_one = dna_to_amino(strand_frames[i])
    proteins_one = protein_sequences(amino_sequence_one)
    # Add proteins to set
    for prot in proteins_one:
        protein_set.add(prot)

    # Acquire the amino acid sequence for the frame and its proteins 
    amino_sequence_two = dna_to_amino(strand_rev_comp_frames[i])
    proteins_two = protein_sequences(amino_sequence_two)
    # add proteins to set
    for prot in proteins_two:
        protein_set.add(prot)

# Print output
proteins = list(protein_set)
for prot in proteins:
    print prot

