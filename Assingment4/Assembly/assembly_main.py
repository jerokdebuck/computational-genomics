#!/usr/bin/python

'''
    Darian Hadjiabadi
    The main driver program for Assignment 4 Question 4
'''

import assembly as assemble


def main():
    # initialize file IO names
    initial_reads = "reads.fa"
    overlap_file_name = "overlaps.txt"
    unitig_file_name = "unitigs.txt"
    solution_file = "solution.fa"

    # Parse fasta file to acquire fa dictionary
    fh = open(initial_reads, "r")
    parser = assemble.Parse_Fasta(fh)
    print 'Parsing .fa file'
    fa = parser.parse()
    print 'Done parsing'
    min_length = 40
 
    # Acquire max overlap graph and print results
    overlap = assemble.Overlap_Graph(fa, min_length)
    print 'Creating maximum overlap graph'
    max_overlap_graph = overlap.create_maximal_overlap_graph()
    print 'Retrieved maximum overlap graph' 
    print_overlap_graph(max_overlap_graph, overlap_file_name)

    # Acquire unitigs and print results
    unitig_instance = assemble.Unitigs(overlap_file_name)
    print 'Building unitigs'
    unitigs = unitig_instance.create_unitigs()
    print 'Unitigs built'
    print_unitigs(unitigs, unitig_file_name)

    # Assemble unitigs and print the genome in fasta format
    assembler = assemble.Assembler(unitig_file_name)
    print 'Assembling genome'
    genome = assembler.assemble(fa)
    print 'Genome assembled'
    print str(len(genome)) + ' is the length of the genome'
    print_genome_fasta(genome, solution_file)

def print_overlap_graph(overlap, overlap_file_name):
    f = open(overlap_file_name, "w")
    for over in overlap.keys():
        f.write(over + ' ' + overlap[over][0] + ' ' + str(overlap[over][1]) + '\n')
    f.close() 

def print_unitigs(unitigs, fn):
    f = open(fn, "w")
    i = 0
    for k in unitigs.keys():
        f.write("START UNITIG {0} ".format(str(i+1)))
        f.write(k + "\n")
        for fr in unitigs[k]:
            f.write(fr[0] + ' ' + str(fr[1]) + "\n")
        f.write("END UNITIG {0}".format(str(i+1)) + "\n")
        f.write("\n")
        i += 1
    f.close()

def print_genome_fasta(genome, fn, per_line=60):
    f = open(fn, "w")
    offset = 0
    f.write(">solution\n")
    while offset < len(genome):
        nchars = min(len(genome) - offset, per_line)
        line = genome[offset:offset+nchars]
        offset += nchars
        f.write(line + '\n')
    f.close()

if __name__ == '__main__':
    main()
