'''
Darian Hadjiabadi
Useful Classes for Assembly Assignment
'''

# Code based off Langmead's
class Parse_Fasta(object):
    def __init__(self, fh):
        self.fa = {}
        self.name = None
        self.fh = fh
    def parse(self):
        for ln in self.fh:
            if ln[0] == '>':
                name = ln[1:].split()[0] 
                self.fa[name] = []
            else:
                self.fa[name].append(ln.rstrip())
        for name, nuc_list in self.fa.iteritems():
            self.fa[name] = ''.join(nuc_list)
        return self.fa

# Main function is to create an overlap graph        
class Overlap_Graph(object):
    def __init__(self, fa, min_length):
        self.fa = fa
        self.min_length = min_length 
        self.overlap = {}    
    def create_maximal_overlap_graph(self):

        # check all strings to see if there are overlaps >= min_length (40)
        keys = self.fa.keys()
        for i in xrange(0, len(keys) - 1):
            r1 = self.fa[keys[i]]
            for j in xrange(0, len(keys)):
                if j == i: j += 1
                r2 = self.fa[keys[j]]
                # Acquire overlap length between two strings
                overlap_length = self.suffixPrefixMatch(r1,r2,self.min_length)
                if overlap_length >= self.min_length:
                    if keys[i] not in self.overlap:
                        self.overlap[keys[i]] = [(keys[j], overlap_length)]
                    else:
                        self.overlap[keys[i]].append( (keys[j], overlap_length) )
        # Go from overlap_graph -> maximum overlap graph
        self.maximum_overlap_graph = self.overlap_to_maximum()
        return self.maximum_overlap_graph
    # Code based off Langmead's
    def suffixPrefixMatch(self, str1, str2, min_overlap):
        if len(str2) < min_overlap: return 0
        str2_prefix = str2[:min_overlap]
        str1_pos = -1
        while True:
            str1_pos = str1.find(str2_prefix, str1_pos + 1)
            if str1_pos == -1: return 0 
            str1_suffix = str1[str1_pos:]
            if str2.startswith(str1_suffix): return len(str1_suffix)
            
    def overlap_to_maximum(self): 
        maximal_overlap_graph = {}
        # Iterate through all keys
        for name1 in self.overlap.keys():
            ties = True
            max_overlap = 0
            max_read = None
            # Iterate through the list acquired from overlap[key]
            for i in xrange(0, len(self.overlap[name1])):
                # If currnet overlap equal to max, set ties to True
                if self.overlap[name1][i][1] == max_overlap: ties = True
                elif self.overlap[name1][i][1] > max_overlap:
                    max_overlap = self.overlap[name1][i][1]
                    max_read = self.overlap[name1][i][0]
                    ties = False
            # Only output if there is no tie between max overlaps
            if ties == False:
                maximal_overlap_graph[name1] = (max_read, max_overlap)
        return maximal_overlap_graph

# A class to create unitigs
class Unitigs(object):
    def __init__(self, file_name):
        # From file create a dictionary that maps A -> B
        # and one that maps A -> overlap length
        self.right_best_buddies = {} 
        self.maximum_overlap = {}
        f = open(file_name, "r")
        self.right_best_buddies = {}
        self.maximum_overlap = {}
        for line in f.readlines():
            r_read, l_read, overlap = line.strip().split(' ')
            self.right_best_buddies[r_read] = l_read
            self.maximum_overlap[r_read] = overlap

    def create_unitigs(self):
        # All unitig headers do not appear in the values in dictionary
        # mapping A -> B (right best buddies)
        roots = [] 
        for k in self.right_best_buddies.keys():
            if k not in self.right_best_buddies.values():
                roots.append(k)

        # Initialized a list call used that will keep track of which
        # nodes have been called on prior
        used = []
        unitigs = {}
        # Iterate through each root
        for root in roots:
            curr_key = root
            overlap = self.maximum_overlap[root]
            unitigs[curr_key] = []
            temp_key = curr_key
            # Loop until either the value is not a key
            # or if it is but has already been used
            while True:
                # Acquire the current value of the temp_key
                v = self.right_best_buddies[temp_key]
                if v not in self.right_best_buddies.keys(): 
                    if v not in used:
                        # add to unitigs dictionary
                        used.append(v)
                        unitigs[curr_key].append( (v, self.maximum_overlap[temp_key])  )
                        break
                else:
                    if v not in used:
                        used.append(v)
                        unitigs[curr_key].append( (v, self.maximum_overlap[temp_key]) )
                        # after adding, change temp_key to v since v was
                        # present in the keys
                        temp_key = v             
                    else:
                        break
        self.unitigs = unitigs
        return self.unitigs

# A class to assemble unitigs 
class Assembler(object):
    def __init__(self, file_name):
        self.f = open(file_name, "r")
    # Go through each unitig, putting the pieces together
    def assemble(self, fa):
        genome = []
        unitig_lengths = []
        curr = None
        while True:
            line = self.f.readline()
            if len(line) == 0: break
            line = line.strip().split(' ')
            if line[0] == "START":
                    
                curr = line[-1]
                genome.append(fa[curr])
                while True:
                    line = self.f.readline().strip().split(' ')
                    if line[0] == "END": 
                        break
                    curr = line[0]
                    overlap = int(line[1]) 
                    genome.append(fa[curr][overlap:])
        
        return ''.join(genome)
