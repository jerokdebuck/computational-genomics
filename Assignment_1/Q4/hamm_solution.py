#!/usr/bin/python
import sys

def calc_hamm_distance(seq_one, seq_two):

    if (len(seq_one) != len(seq_two)):
        print('Cannot compare strings of unequal length')
        return 
    if (len(seq_one) > 1000 or len(seq_two) > 1000):
        print('Lengths of one or more greater than 1000')
        return
    hamm_distance = 0
    for i in range(len(seq_one)):
        if seq_one[i] != seq_two[i]:
            hamm_distance += 1
    return hamm_distance

line_list = []
for lines in sys.stdin.readlines():
    line_list.append(lines[0:len(lines) - 1])
hamm_distance = calc_hamm_distance(line_list[0], line_list[1])
print hamm_distance
 
