#!/usr/bin/python
import sys

def transcribe(dna_sequence):
    if len(dna_sequence) > 1000:
        print('sequence longer than 1 kilo basepairs')
        return 
    transcribed = ''
    for nucleotide in dna_sequence:
        if nucleotide == "A":
            nucleotide = "T"
        elif nucleotide == "T":
            nucleotide = "A"
        elif nucleotide == "G":
            nucleotide = "C"
        elif nucleotide == "C":
            nucleotide = "G"
        transcribed = nucleotide + transcribed
    return transcribed

for line in sys.stdin.readlines():
    transcribed_line = transcribe(line[0:len(line)- 1])
    print(transcribed_line)
