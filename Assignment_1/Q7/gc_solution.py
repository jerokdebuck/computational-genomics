#!/usr/bin/python
import sys

def gc_content(sequence, num_sequences):
    if num_sequences > 10:
        print('only 10 sequences allowed')
        return
    if (len(sequence) > 1000):
        print('length of sequence larger than 1000')
        return
    total = len(sequence)
    gc = 0
    for nucleotide in sequence:
        if nucleotide == "G" or nucleotide == "C":
            gc += 1
    return 100*(gc /  float(total))

def find_highest_content(content_map):
    highest = 0
    position = 0
    for k, v in content_map.iteritems():
        if v > highest:
            highest = v
            position = k
    return position, highest

sequence_content_map = dict()
key = ''
current_sequence = ''
num_sequences = 0
for lines in sys.stdin.readlines():
    if '>' in lines:
	if len(current_sequence) > 1:
            num_sequences += 1
            sequence_content_map[key] = gc_content(current_sequence, num_sequences)
        current_sequence = ''
        key = lines[1:len(lines)-1]
    else:
        current_sequence = current_sequence + lines[0:len(lines) - 1]
# have to find gc count of last sequence
num_sequences += 1
sequence_content_map[key] = gc_content(current_sequence, num_sequences)
key, value = find_highest_content(sequence_content_map)
print key
print value
          
