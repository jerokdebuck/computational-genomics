>Rosalind_8690
CGCTGATGTGTTTCAACGGCGCTAGACACTAACGGGAAGAGTAATTCTCCGAGGCGGTAA
GAGATACTATGAAGTTTAGTTATCGAACTTACCAAGTAGGCTGTCCAGTCTAGATGAAAG
AGTAGTCACTAGTCGCACCACGCGACCAGTACGTCACACGTCCACAGTGCGTCTGGCAAC
GGAGTGCCTGGATTGTTCTCTAATCGGGCTCACCCCCTTGGATTGTCAGTATGGTGTACC
GTGAAACCTATCAATGTCTAGAAATCTATGGGCGTTACGATCTGTTCGTGGGTAGTCGTG
TGTGTCTCATGACCATAGACGTTACTGATTAACCATGCAGGGTAGCTTCTCAGGTAAAGC
ATCCATAGTTCGGGAAGCTACGACTGGCTATTGCACGGGACAATCATCTGTGGGGACTGT
CCTCGCTAGGGTGCACTTTATAGCGCCGTCTCTGGGCATACTTTTGAACATCGCGCCGGC
CCCGTGAAGGAACAACCGCTTCGTAGAGGACGACCAAGCACACCGTGAGATGAGGGGGAT
TATTACTGTGGAACCGCTAGGACATGTTTTCGCAGCCGTTCGGTAGCCCTGTTATAATAT
CACGCGCCACCCGACTCTTGTACGTCAAAATTATAGATAAGTCAAAGCGACAAACTCTTC
GGTAATAGCCGTTAATCGATGCGTCTATTGGGCATTACACGGATGCGAGTCTAGACCTAA
TCAGAGATGAATCCCTTACCTCGTTGATTGAACTTTGCACGGAACAGACGCCAGGCTGGT
AGCCAACAGCCACCGATCGCAAGAAATAAATACCACATTGGCTTTGCCCGGAGCAGTGAG
ATAGTATCAATCTGGCGTCTGTAGCATCATGTGACCTATAGTGGGAGGGTCATTCTGTTG
CCGTACCTTGATGACTCTACTT
>Rosalind_7991
GATGTTGTATACGGTGCTCGGCACGATAACATGTTGTGGTATCGGCACGTATCTCGATCC
ACAAGACGTCACCTGGCGTAAAGTTAACACAAAGATCCGGAGGACTAGCGCATCGAAAAT
ACGATACGGGTGGACAGGGTGGCCGCGTTCCCTGAATGACTCCCTCTGCTTTTTATATGG
AGAACTGACTGGTTAGTGTAATCACACGGCCCGCAGTCAAAGCATCTCAGATCATTAATC
AAGAGCATCTATCCGTTTAGGTTAGTTTGTGGCAGTCTTAGCAATCAATAGTTCGAAAGA
GTTACCCCTATTACGGGGATGTCTAGCCCAGGTAATTCACTGACTACAGCGCTTTAGATA
GCGCAAAGAACTCGCAGACGAATCCAGACGCTCGTATACAAGAAATTCGATTCAACTCAG
CGTAGACGCGGTTATGAAAATCTGTTGTAGCATTTACCAGCAGAATCGTAAATAAATGAA
AAGTGGAAGCGTTTCAAGCCCCTGTCCGCCATGCCTCTCGGTAGAGTTTACATCTGGCCG
TAACAAGGGTTATTTTACTACGCTGCAGCACGCCGGAACCCGAGGGGACCTCCGATCTAA
GTAAGCTTTTGCAATCTTAGGACTGCGGCATGTGCCGAGGTGATTACTGCTGCTTGTGCC
AGGGCTGTCAATGCGTTGACAACCGGTTCGCCAACAGACCCGTTCAGAAATCATAGTCCA
ACTCGGGTCCTCAGTCGATCCGCCCGTAGTGCCTGACCCCTCATTGCAGTGTGCCTCGCT
ATGCTTAGTTCACCCACAAGAGCCCACTCGTGGCTTTCGTCGCGTCTACTATATAAGTGT
GGCGGAGTCGAGCTAAGTGCTCCCCGAACATAT
>Rosalind_1096
GGCCAGGCCGGTAAGATGTACGCATGCTGTCTTGCATTCCCCTGCTTCTCGCGTCGCAAC
CATTTACTTGAAGCGCGAGGTGCGTGTTCTATCAGGTGCACATCGATCGGCGAAGTTGCT
ATTTCGACTGGAGAGTAGTACGCCGGAACTATGCCTGTTTAGTTTACCCGGTACCAAGTG
ATCTACCTTAAGGGGGAAGGAGGGAAGGTTCGTTGGGTTACCCCTATGGTGGCGTACGAC
TCCCTGACTATGGCTGTACCCTGAGATACACTACACATAGCCGGTAGCTCGGCATGTGAC
CATGACGCCGGCTATCGTTCCAGCCCGTCCTCGATGTGCCGGCAATTGATTGTTCTCTCT
GTTAGGATTATGAGCAGCTTCGAGCTGTCTAGTGGCTGTACGCGGAACAGCAACCACACC
CTGATACTGCCTTAAACTCGGGGCAAATGGGGTACCGATGCTTGGAAACAGTCACTCACG
CGAGGCTACGTTACGTGTCCGCTGGGATATCTGCCCCCCGGAAGCCGTTCTGGTAGGTCT
GACCCTACCAATCAAAGCCTCAAAAGCTTGTAGACTGCTATTGAGCTAAGGTAGAATGGC
TGGCCCCCCATACGATGCAAGGACTGGGAGTTTCGTCTCAGCTCTTAACCCGCGGCGATC
AGTGGAAAGAACATAGGACCCGAAGCGATATAGTAACGTTGAGGGTATGGTTAGTTAGGC
TTTGGAGGGCATTAAGTATGGGTTGCACATTCGAACCCACACTACGCGCGACATACGTCT
CTGACTAAGTGAAGGTCTTGCACACTCAAGTTTCAAGAGGCGATCGGATATTCCTCTGGC
GACTACGTTTATCCATAAGGTCCTAACTT
>Rosalind_5597
CGCACGCCTTACCAACGAGTGGGAGTTGGTTTATAGCTTAGCTGAGTTCCACCACCTCGG
GAGGCCAGCCTTCGGTCGTAAGCCGGGGGACCTTTCTCGTCTCGTTCCAATACGGAACGC
CCTTTATCTGGTCATGTACTCGATGTCCCACCTCTTGATGCTCGCAGTGCTCGTCCCTCT
GCGGGTCAAGGCTCAACTGAATCTTACCCACAACTATAGTAACAATCATAGTTTTGGCAG
TACGTTGTGTTAGTTTCGCAAGGCCAAGCAAAGCGGCTATATAACTCGGGGTATGAAATA
CAAGCTTCCTTCGAACACTTGTGGATTCAAGCCGGAGCTTGTCGTACTTACCGGTCTTAT
GACGCGATGAGGGTTGGTATGACACAGCTATTGTGAGGATGTGCGTATGCGCCGGGTAGG
CGACAATCACTGTGCCCAACACTATACTAGTTTGGGGACGCGCGCATATTTGGTTGCCCA
CATCCCAGCCCCTTCTAAAGCAAACGTTTCGAAGCTCGTTGCCTAGCCACAATTTGAACT
AACCCGCTACCAATGATGCGGGCGGGTCGCCCCGTGTAATGGATCTGTAACGCACATACA
TAATTCAATACAGACAATGACCCAAAAATGTCAAAGTCTCAGATGCCATACTGCTACAGG
ATGTTCTAGCGTGATACGACGTCGTGATGTGAAATGGGACAAGCAGATATAACCGCCGTG
GGGGGCATCACGCGGAGGTGTTATGCTTGCAGCACGGAATTCGGCGCTTGTTGGTAAATA
GGACGAAATGCTAGTCCGCTACGTCCAGACGAACTCGT
>Rosalind_2842
TGCTGTTATTAACAACCATTGCGCCATATAGGCCGCATGCGCGCGGCCTTTGGATCGTTT
TCAACTGGGTGGACAACATAGGATAACCAGCTTAGACTTAACGGCTCCTCTATTTCACCG
GCTACAGATATAGAGAAATAATAGCGTTACAGTTATGCACGTCGGGTCATGTTATTGGCA
CGCAACGCAAGTATCGTAATACATACAAGTCATAGTGTTAATAACGGGCTGTTGGATAAC
CGGACTTATAGCTCAGGTGTTATCTATCCCAGTGGTGAGGCGTCAGCCTACTCGATTTCA
AGACCTATGTGCATGTGCTAAGTACATACGAGTAGGGTAATCTGCCAAGCAACCCCTTAT
ATTTTCATTCCCGGTGATTTACACTTACAAGAAATGACCTTTGTATCGGAAAAGTATATT
GACCCGGACATTAGGTTTCTACACGGTAAGATAAGCTCCTACCGAATCCGAGGGCAGCAA
ATTTCCAGTGCGGGAATCATTACACTTGCAGATTCTGCAAGGACCGCAGGGATTTTCAGT
CATGTAAAGGCGTTCGAAGCAGTCTATACGAGATTGGCGCGTGCCCGGTCATTCCGGGAG
CGTCAAACGCGCAAGTTTGTCATGCATGAACAGTGATTCACACGGACGGGACCTGCGTCA
TGTCTGTTCACAGAAGCCTACCATGTTTCGTGAACGGTGCATAGGAACCGGAAAAGGAGC
CATGGTATAGATTGAAAAGCCGTCTTTAGCACTCTTTAAGCATATGACAGATCCTAAACG
AGAAGTTTGTCCATATCCGCTGCAGCTCGCGAGTGGTGGTCATATAAGCTATAGATTTGG
TCCATCACAGCGTGGGCATCGATCAGGTAGCTCGTCTATGGTCCGGGAAGGCGATGAAAG
AAGTAAAATTT
>Rosalind_6728
CGGGGCAATGCTGCTCGGAATTCCGCTATGGAACATTTCCTCTGCTATAACACCAGAGGG
GCCCAGAGAGTTGCAAGGGTAGATTCATAATTCATTAGAAAGGACGCTAGGAAGGGAAAC
CAGCGTAGCGCGGAACGTCACTGGATTAGTACGCCAGTTAGGTTACCGGCCGACATGCAA
TTCGCGAGTGGCCGCGTGCCACCGCATTCGGAGTAAGAAGCCTGTTTCCCATGCCCGTAG
TCTCAGCATGACTAGCCCTTGATGATCCTGTGCAGAGCATGATGTAAATACGGGGGACGC
GTTGCACGGTGATAGGACGGGGGAGAAGCCATGTCCTACGAGGCCTATCCAACGGGATCA
GGATATGCAGTCGCACTGTATCTCAAGAACGCCTCGACCGTCAGTCTGACGACCTCTAAG
ATAGGAATGATGATCTGAGTAAAGAGGAATGCTGGTAGAGGATTCCCTGAACCGCGTATC
TCTTGTCCGAGTGCCCCGAACGTACTGTTAATGTGCCGTGGGTTCCTAATGCTAATGCGA
CTCCACGGGCAAGGGGAAGGCCACAATATAGGCCCAGCCGTTGGTTCGAACACGATAAAT
ATTCATGTGCCCACTGAAGCAGAGAATCGGTTGCCCGGACCTGTGAAGATGATGCCAGGA
TACGCACTAAATGGTGACTAGCATTTGAGGAACTTCTTCAATTCCAGTATGTATTTGATA
GACTTAGAAAGGGACACCTGAAGCGGGCCGAATGACCCGCGACGCTTTTAATACCGTATT
TCAGCTTAGTTACTGTACCAAACAGCACGTAGCCCCAGGTAGTCTAAGGCTCATCCGATC
TTTGTGGTAGATGCTATCATTGATCAGGGGGATTACACAAAATTATTTTGCTACTTAACA
TTTCGAGTCACTACGTAAGGCCCGT
>Rosalind_3116
GCACCCAGGGTGTAGCGTAGGGATAGCAGGAATGCGCATATACGCGTACATAGGTCCAGA
ACTTCGGATTTCGATCGCCGACTCCTGTAGTGTCGCCCTAAGATTCACCTAGTTGTAAAT
AGCTCCATTAGTGCGATCGACGCGTAAGACACCACCAGCAGAGTCCGGTATATTGGTCAC
GATCCCTACGCGGAGGTAGCCTAATCACGCTGTGGTACCTGAACCATCACGCGGGTCTTC
CTCGAATTTGAACTGACCGGGCCCGGTACCCTGTATTTGGCCAACCTGTAAGATTATCCG
TCAGGTTGGACTGTATACGTCATACTATCGGGCAAAGGAGCAATACGGCCCTTCATTGTA
CCAGCCCGGTTCAGTCTGTACATGACCAATTGTCTCAGCCAGCTTTGTCAGGAGTTGGTG
GGGACTCGCTGGTAAGCCTTTCCGACTGGCAAACTGCCTAAGGTTTGGCAGAACGCATAC
GACGTCTACCCTGCAGAGTGGACACGTTATTGCAGGTTCGTGAAGGCAACTCTACCTCTT
CCCCTCTAATCTCAGTACGGATGCATTATTCGTCATTCGTAACCTTTAGTAATATAATTT
CATGGGCTCTTGCTGGGGGACGGACGAGGGATTCCTCGGAGCCTAGTTTGAAGCTACAAG
TTTCATCTTAGTCTATACCCTCCTACGGAGCTTGATTTGCCGCGCAATAAGCCAAACTCT
TCGCTGTATATCTCATAGGGGTCAAATTGATCATGCCCTTCGCCGAAGATCTTGAGAGTT
CAGGACCTTTGCTCCGGCCCGTTGTCTTGTTAAGTAACAAGGGTGGTACGTGGGGAGCAC
GCGGTAATGAACTTGATCACCCTCTTCTAATCGATACCCGGTTAAAGCCACGTC
