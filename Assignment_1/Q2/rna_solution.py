#!/usr/bin/python

import sys 

def dna_to_rna(dna_sequence):
    if len(dna_sequence) > 1000:
        print('sequence longer than 1 kilobase pair')
        return 
    rna = ''
    for nucleotide in dna_sequence:
        if nucleotide == "T":
            rna = rna + "U"
        else:
            rna = rna + nucleotide
    return rna

for line in sys.stdin.readlines():
    rna = dna_to_rna(line[0:len(line) - 1])
    print(rna)
