#!/usr/bin/python

import sys
import matplotlib.pyplot as plt

Q = (5, 20, 40)
for q in Q:
    x = 10.0 ** (-1 * q)
    print(q, x)

greater = []
less = []

for line in sys.stdin.readlines():
    line = line.rstrip().split(' ')
    greater.append(line[-1])
    less.append(line[-2])
plt.figure()
plt.plot(greater, color='r', marker="+")
plt.plot(less, color='b', marker="+")
plt.legend(["phred value greater than 20", "phred values less than 20"], loc="center left")
plt.xlabel("Row #")
plt.ylabel("Number")
plt.title('Phred value over length of read for 1000 sequences')
plt.show()



