#!/usr/bin/python

import sys

def parse_fastq():
    reads = []
    while True:
        first_line = sys.stdin.readline()
        if len(first_line) == 0:
            break
        name = first_line[1:].rstrip()
        seq = sys.stdin.readline().rstrip()
        rows = len(seq)
        sys.stdin.readline()
        qual = sys.stdin.readline().rstrip()
        reads.append((name, seq, qual))
    return reads, rows

def print_summary(reads, position):
    a_count = 0
    t_count = 0
    g_count = 0
    c_count = 0
    other_count = 0
    
    phred_threshold = 20
    phred_less = 0
    phred_greater = 0
  
    for i in range(len(reads)):
        bp = reads[i][1][position]
        if bp == "A":
            a_count += 1
        elif bp == "T":
            t_count += 1
        elif bp == "G":
            g_count += 1
        elif bp == "C":
            c_count += 1
        else:
            other_count += 1
        phred_char = reads[i][2][position]
        phred_int = ord(phred_char) - 33
        if phred_int < phred_threshold:
            phred_less += 1
        else:
            phred_greater += 1
    print a_count, c_count, g_count, t_count, other_count, phred_less, phred_greater

reads, rows = parse_fastq()
for i in range(rows):
    print_summary(reads, i) 
