#!/usr/bin/python
import sys

def count_nucleotides(sequence):
    if len(sequence) > 1000:
        print('sequence longer than 1 kilo basepairs')
        return 
    a_count = 0
    t_count = 0
    g_count = 0
    c_count = 0
    for nucleotide in sequence:
        if nucleotide == "A":
	    a_count += 1
	elif nucleotide == "T":
	    t_count += 1
	elif nucleotide == "G":
	    g_count += 1
	elif nucleotide == "C":
	    c_count += 1
    return a_count, c_count, g_count, t_count

for line in sys.stdin.readlines():
    a, c, g, t = count_nucleotides(line[0:len(line) - 1])
    print a, c, g, t
