#!/usr/bin/python

import sys


def codon_table(codon):

    c_table = {"UUU":"F", "UUC": "F", "UUA":"L", "UUG":"L", "UCU":"S", "UCC":"S",
    "UCA":"S", "UCG":"S", "UAU":"Y", "UAC":"Y", "UAA": 0, "UAG": 0, 
    "UGU":"C", "UGC":"C", "UGA": 0, "UGG":"W", "CUU":"L", "CUC":"L",
    "CUA":"L", "CUG":"L", "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q", "CGU":"R", "CGC":"R",
    "CGA":"R", "CGG":"R", "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T", "AAU":"N", "AAC":"N",
    "AAA":"K", "AAG":"K", "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R", 
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V", "GCU":"A", "GCC":"A",
    "GCA":"A", "GCG":"A", "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}
    return c_table[codon]

def translate(sequence) :
    if len(sequence) > 10000 or len(sequence) % 3 != 0:
        print('Cannot translate')
        return
    codons = []
    for i in range(0, len(sequence), 3):
        codons.append(sequence[i:i+3])
    amino_acids = ''
    stopped = False
    for codon in codons:
        acid = codon_table(codon)
        if acid == 0:
            stopped = True
            break
        else:
            amino_acids = amino_acids + str(acid)
    if not stopped:
        return 0

    return amino_acids

for line in sys.stdin.readlines():
    amino_acid_sequence = translate(line[0:len(line) - 1])
    if amino_acid_sequence == 0:
        print('No stop codon found')
    else:
        print amino_acid_sequence
 
