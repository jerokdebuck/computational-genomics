#!/usr/bin/python

import sys


def motif_locations(s, t):
    if len(t) > len(s):
        print('lengths of strings not correct')
        return 
    if (len(t) > 1000 or len(s) > 1000):
        print('lengths of one or both strings greater than maximum allowed')
        return
    substr_position = []
    for i in range(len(s) - len(t) + 1):
        match = True
        for j in range(len(t)):
            if s[i + j] != t[j]:
                match = False
                break
        if match:
            substr_position.append(str(i + 1))
    return substr_position


sequences = []
for line in sys.stdin.readlines():
    sequences.append(line[0:len(line) - 1])

position = motif_locations(sequences[0], sequences[1])
for pos in position:
    sys.stdout.write(pos + ' ')
