#!/usr/bin/python
import sys

# Inverted substring index using hash table. 
# class IndexHash from Ben Langmead's github repository
# with some additions to extract subsequences
class IndexHash(object):

    def __init__(self, t, ival, mask):
        self.t = t
        self.ival = ival
        self.mask = mask
        self.index = {}
        for i in xrange(0, len(t) - len(mask) + 1): #
            substring = t[i:i+len(mask)]
            substr = ''
            # This may be a naive way of masking as it seems to slow down the process of building the index quite a bit
            for j in range(len(mask)): # iterate through mask
                if mask[j] == '0': substr = substr + ' ' # if 0 add a space
                else: substr = substr + substring[j] # else add the char at index j
            if substr in self.index:
                self.index[substr].append(i)
            else:
                self.index[substr] = [i]
    def query(self, p):
         return self.index.get(p, [])

def query_partitions(pattern, mask, index):
    partitions = []
    for i in xrange(0, len(pattern) - len(mask) + 1):
        substring = pattern[i:i + len(mask)]
        substr = ''
        for j in range(len(mask)): # same idea as above
            if mask[j] == '0': substr = substr + ' '
            else: substr = substr + substring[j]
        partitions.append(substr)
    indices = []
    for partition in partitions:
        indices.append(index.query(partition)) # query partition and add index hits to indices list
    return indices, partitions

def check_partitions(partition_matches, t, p, partitions, mismatches):
    # code heavily based off lecture notes from Ben Langmead
    occurances = set() # a set that list indexes of approx matches
    exact_matches = 0
    approx_matches = 0
    offset = 0
    for i in range(len(partition_matches)): # loop through all partition index hits
        for j in range(len(partition_matches[i])): # in each partition list index through all indices
            if partition_matches[i][j] - offset < 0: continue 
            if partition_matches[i][j] + len(p) - offset > len(t): continue 
            mm = 0
            for x in range(0, offset) + range(offset+len(partitions[i]), len(p)):
                if t[partition_matches[i][j] - offset + x] != p[x]: # check to see how many edits we have
                    mm += 1
                    if mm > mismatches: break # stop if we overstayed our welcome
            if mm <= mismatches:
                if mm == 0 and partition_matches[i][j] - offset not in occurances: # exact match if mm = 0 and index not in set yet
                    exact_matches += 1 
                occurances.add(partition_matches[i][j]-offset) # add index to set
        offset += 1 # incriment offset
    return exact_matches, len(list(occurances))
T = sys.stdin.read()
substr_len = 6
mismatches = 1
mask = '10101010101'
index = IndexHash(T, 1, mask)
words = ["achievements", "acquaintance", "remembrances"]
for word in words:
    partition_matches, partitions = query_partitions(word, mask, index)
    # Get total number of index hits, the denominator of specificity
    total_hits = 0
    for i in range(len(partition_matches)):
        total_hits += len(partition_matches[i])
    exact_matches, approx_matches = check_partitions(partition_matches, T, word, partitions, mismatches)
    # Report the word, number of exact matches, number of approximate matches, and specificity
    print('Word: ' + word)
    print('Exact Matches: ' + str(exact_matches))
    print('Matches with <= ' + str(mismatches) +' edit(s): ' + str(approx_matches))
    print('Matches with ' + str(mismatches) + ' edit(s): ' + str(approx_matches - exact_matches))
    print('Specificity: ' + str(float(approx_matches)/total_hits))
    print('=======================================')
