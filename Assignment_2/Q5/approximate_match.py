#!/usr/bin/python
import sys

# Inverted substring index using hash table. 
# class IndexHash from Ben Langmead's github repository
class IndexHash(object):

    def __init__(self, t, ln, ival):
        self.t = t
        self.ln =ln
        self.ival = ival
        self.index = {}
        for i in xrange(0, len(t) - ln + 1):
            substr = t[i:i+ln] # extract substrings of length ln (6 for this problem)
            if substr in self.index:
                self.index[substr].append(i)
            else:
                self.index[substr] = [i]
    def query(self, p):
         return self.index.get(p, [])

def query_partitions(pattern, index, mismatches):
    # split word into mismatches + 1 disjoint partitions
    num_partitions = mismatches + 1
    cut_off = len(pattern) / num_partitions # length of each partition
    partitions = []
    indices = []
    for i in range(num_partitions):
        substr = pattern[i*cut_off:cut_off*i+cut_off] # acquire the partition
        partitions.append(substr) # append to partitions list
        indices.append(index.query(substr)) # query partition and add index hits to indices list
    return indices, partitions

def check_partitions(partition_matches, t, p, partitions, mismatches):
    # code heavily based off lecture notes from Ben Langmead
    occurances = set() # a set that list indexes of approx matches
    exact_matches = 0
    approx_matches = 0
    offset = 0
    for i in range(len(partition_matches)): # loop through all partition index hits
        for j in range(len(partition_matches[i])): # in each partition list index through all indices
            if partition_matches[i][j] - offset < 0: continue 
            if partition_matches[i][j] + len(p) - offset > len(t): continue 
            mm = 0
            for x in range(0, offset) + range(offset+len(partitions[i]), len(p)):
                if t[partition_matches[i][j] - offset + x] != p[x]: # check to see how many edits we have
                    mm += 1
                    if mm > mismatches: break # stop if we overstayed our welcome
            if mm <= mismatches:
                if mm == 0 and partition_matches[i][j] - offset not in occurances: # exact match if mm = 0 and index not in set yet
                    exact_matches += 1 
                occurances.add(partition_matches[i][j]-offset) # add index to set
        offset += len(partitions[i]) # incriment offset
    return exact_matches, len(list(occurances))
 
T = sys.stdin.read()
substr_len = 6
mismatches = 1
index = IndexHash(T, substr_len, 1)
words = ["achievements", "acquaintance", "remembrances"]
for word in words:
    partition_matches, partitions = query_partitions(word, index, mismatches)
    # Get total number of index hits, the denominator of specificity
    total_hits = 0
    for i in range(len(partition_matches)):
        total_hits += len(partition_matches[i])
    exact_matches, approx_matches = check_partitions(partition_matches, T, word, partitions, mismatches)
    # Report the word, number of exact matches, number of approximate matches, and specificity
    print('Word: ' + word)
    print('Exact Matches: ' + str(exact_matches))
    print('Matches with <= ' + str(mismatches) +' edit(s): ' + str(approx_matches))
    print('Matches with ' + str(mismatches) + ' edit(s): ' + str(approx_matches - exact_matches))
    print('Specificity: ' + str(float(approx_matches)/total_hits))
    print('=======================================')
