#!/usr/bin/python
import sys
import collections

# Inverted substring index using hash table. 
# class IndexHash from Ben Langmead's github repository
class IndexHash(object):

    def __init__(self, t, ln, ival):
        self.t = t
        self.ln =ln
        self.ival = ival
        self.index = {}
        for i in xrange(0, len(t) - ln + 1):
            substr = t[i:i+ln] # extract substrings of length ln (30 for this problem)
            if substr in self.index:
                self.index[substr].append(i)
            else:
                self.index[substr] = [i]
    def query(self, p):
         return self.index.get(p, [])

def query_partitions(pattern, index, mismatches):
    # split word into mismatches + 1 disjoint partitions
    num_partitions = mismatches + 1
    cut_off = len(pattern) / num_partitions # length of each partition
    partitions = []
    indices = []
    for i in range(num_partitions):
        substr = pattern[i*cut_off:cut_off*i+cut_off] # acquire the partition
        partitions.append(substr) # append to partitions list
        indices.append(index.query(substr)) # query partition and add index hits to indices list
    return indices, partitions

def check_partitions(partition_matches, t, p, partitions, mismatches):
    # code heavily based off lecture notes from Ben Langmead
    occurances = set() # a set that list indexes of approx matches
    exact_matches = 0
    approx_matches = 0
    offset = 0
    lowest_mismatches = mismatches + 1 # for each read, find the lowest number of mismatches in the reference genome
    # set it to mismatches + 1 for convenience
    for i in range(len(partition_matches)): # loop through all partition index hits
        for j in range(len(partition_matches[i])): # in each partition list index through all indices
            if partition_matches[i][j] - offset < 0: continue 
            if partition_matches[i][j] + len(p) - offset > len(t): continue 
            mm = 0
            for x in range(0, offset) + range(offset+len(partitions[i]), len(p)):
                if t[partition_matches[i][j] - offset + x] != p[x]: # check to see how many edits we have
                    mm += 1
                    if mm > mismatches: break # stop if we overstayed our welcome
            if mm <= mismatches:
                if mm == 0 and partition_matches[i][j] - offset not in occurances: # exact match if mm = 0 and index not in set yet
                    lowest_mismatches = 0
                    exact_matches += 1 
                if mm < lowest_mismatches:
                    lowest_mismatches = mm # even if partition_matches[i][j] - offset is in occurances
                    # there could be the chance where the number of mismatches is lower than prior
                occurances.add(partition_matches[i][j]-offset) # add index to set
        offset += len(partitions[i]) # incriment offset
    return exact_matches, list(occurances), lowest_mismatches

def parse_reference(reference):
    # Read reference genome, get rid of first line, and strip the rest
    f = open(reference, 'r')
    f.readline()
    reference = ''
    for line in f.readlines():
        reference += line.strip()
    return reference

def parse_reads(reads):
    # parse FASTQ file
    f = open(reads, 'r')
    read_list = []
    while True:
        first_line = f.readline()
        if len(first_line) == 0: 
            break
        name = first_line[1:].rstrip()
        seq = f.readline().rstrip()
        f.readline()
        rows = len(seq)
        qual = f.readline().strip()
        read_list.append((name, seq, qual))
    return read_list, rows
        
def find_overlap(matches_list, read_list, reference_genome, rows):
    overlap = {}
    for i in range(len(matches_list)):
        for index in matches_list[i]:
            for j in range(rows): # for each index that we found an approxiamte match, iterate through the read mapping to the match
                reference_nucleotide = reference_genome[index + j] # get reference nucletide
                if index + j in overlap:
                    overlap[index + j].append((read_list[i][1][j], read_list[i][2][j])) # add the read nucleotide and its phred value
                else:
                    overlap[index] = [(read_list[i][1][j], read_list[i][2][j])]
    return overlap

def print_output(overlap, num_reads, reference_genome):
    output_dictionary = {}
    for k in overlap.keys():
        reference_nuc = reference_genome[k]
        all_same = True
        for nucs in overlap[k]:
            if nucs[0] != reference_nuc:
                all_same = False
                break
        if all_same == False:
            output = calc_weight(k, overlap[k], reference_nuc)
            if output != 0:
                output_dictionary[output[0]] = output
    # order the indexes so that it reads pretty
    ordered_output = collections.OrderedDict(sorted(output_dictionary.items()))
    for k in ordered_output:
        for item in ordered_output[k]:
            sys.stdout.write(str(item) + ' ')
        sys.stdout.write("\n")

def calc_weight(offset, overlap_list, ref_nuc):
    T_weight = 0
    G_weight = 0
    C_weight = 0
    A_weight = 0
    for nucs in overlap_list:
        nuc = nucs[0]
        phred = ord(nucs[1]) - 33
        if nuc == "A":
            A_weight += phred
        elif nuc == "T":
            T_weight += phred
        elif nuc == "G":
            G_weight += phred
        else:
            C_weight += phred

    weight_list = {}
    weight_list["A"] = A_weight
    weight_list["T"] = T_weight
    weight_list["G"] = G_weight
    weight_list["C"] = C_weight

    zero_count = 0
    max_weight = -1
    # Check to see if we have other nucleotides besides reference and that the max is not the ref nucleotide
    for k in weight_list.keys():
        if weight_list[k] == 0:
            zero_count += 1
        if weight_list[k] > max_weight:
            max_weight = i
    if zero_count == 3:
        return 0
    if (ref_nuc == "A" and max_weight != 0) or (ref_nuc == "T" and max_weight != 1) or (ref_nuc == "G" and max_weight != 2) or (ref_nuc == "C" and max_weight != 3):
        max_nucleotide = '' 
        max_weight = -1
        for k in weight_list.keys():
            if weight_list[k] > max_weight:
                max_nucleotide = k
                max_weight = weight_list[k]
        weight_list[max_nucleotide] = -1
        second_nucleotide = ''
        second_weight = -1
        for k in weight_list.keys():
            if weight_list[k] > second_weight:
                second_nucleotide = k
                second_weight = weight_list[k]
        return (offset, max_nucleotide, max_weight, second_nucleotide, second_weight, ref_nuc)
    
  
assert len(sys.argv) == 3
reference_genome = parse_reference(sys.argv[1])
read_list, rows = parse_reads(sys.argv[2])

substr_len = 30
mismatches = 4
index = IndexHash(reference_genome, substr_len, 1) # build index
approx_matches_list = []
for read in read_list: # iterate through all reads
    partition_matches, partitions = query_partitions(read[1], index, mismatches)
    total_hits = 0
    for i in range(len(partition_matches)):
        total_hits += len(partition_matches[i])
    exact_matches, approx_matches, lowest_mismatches = check_partitions(partition_matches, reference_genome, read[1], partitions, mismatches)
    approx_matches_list.append(approx_matches)

overlap = find_overlap(approx_matches_list, read_list, reference_genome, rows)
print_output(overlap, len(read_list), reference_genome)
