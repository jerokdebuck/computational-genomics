#!/usr/bin/python
import sys

# Inverted substring index using hash table. 
# class IndexHash from Ben Langmead's github repository
class IndexHash(object):

    def __init__(self, t, ln, ival):
        self.t = t
        self.ln =ln
        self.ival = ival
        self.index = {}
        for i in xrange(0, len(t) - ln + 1):
            substr = t[i:i+ln] # extract substrings of length ln (30 for this problem)
            if substr in self.index:
                self.index[substr].append(i)
            else:
                self.index[substr] = [i]
    def query(self, p):
         return self.index.get(p, [])

def query_partitions(pattern, index, mismatches):
    # split word into mismatches + 1 disjoint partitions
    num_partitions = mismatches + 1
    cut_off = len(pattern) / num_partitions # length of each partition
    partitions = []
    indices = []
    for i in range(num_partitions):
        substr = pattern[i*cut_off:cut_off*i+cut_off] # acquire the partition
        partitions.append(substr) # append to partitions list
        indices.append(index.query(substr)) # query partition and add index hits to indices list
    return indices, partitions

def check_partitions(partition_matches, t, p, partitions, mismatches):
    # code heavily based off lecture notes from Ben Langmead
    occurances = set() # a set that list indexes of approx matches
    exact_matches = 0
    approx_matches = 0
    offset = 0
    lowest_mismatches = mismatches + 1 # for each read, find the lowest number of mismatches in the reference genome
    # set it to mismatches + 1 for convenience
    for i in range(len(partition_matches)): # loop through all partition index hits
        for j in range(len(partition_matches[i])): # in each partition list index through all indices
            if partition_matches[i][j] - offset < 0: continue 
            if partition_matches[i][j] + len(p) - offset > len(t): continue 
            mm = 0
            for x in range(0, offset) + range(offset+len(partitions[i]), len(p)):
                if t[partition_matches[i][j] - offset + x] != p[x]: # check to see how many edits we have
                    mm += 1
                    if mm > mismatches: break # stop if we overstayed our welcome
            if mm <= mismatches:
                if mm == 0 and partition_matches[i][j] - offset not in occurances: # exact match if mm = 0 and index not in set yet
                    lowest_mismatches = 0
                    exact_matches += 1 
                if mm < lowest_mismatches:
                    lowest_mismatches = mm # even if partition_matches[i][j] - offset is in occurances
                    # there could be the chance where the number of mismatches is lower than prior
                occurances.add(partition_matches[i][j]-offset) # add index to set
        offset += len(partitions[i]) # incriment offset
    return exact_matches, len(list(occurances)), lowest_mismatches

def parse_reference(reference):
    # Read reference genome, get rid of first line, and strip the rest
    f = open(reference, 'r')
    f.readline()
    reference = ''
    for line in f.readlines():
        reference += line.strip()
    return reference

def parse_reads(reads):
    # parse FASTQ file
    f = open(reads, 'r')
    read_list = []
    while True:
        first_line = f.readline()
        if len(first_line) == 0: 
            break
        name = first_line[1:].rstrip()
        seq = f.readline().rstrip()
        f.readline()
        rows = len(seq)
        qual = f.readline().strip()
        read_list.append((name, seq, qual))
    return read_list, rows
        
        
    
assert len(sys.argv) == 3
reference_genome = parse_reference(sys.argv[1])
read_list, rows = parse_reads(sys.argv[2])

substr_len = 30
mismatches = 4
lowest_mismatch_list = [0 for i in range(mismatches+1)]
index = IndexHash(reference_genome, substr_len, 1) # build index
for read in read_list: # iterate through all reads
    partition_matches, partitions = query_partitions(read[1], index, mismatches)
    total_hits = 0
    for i in range(len(partition_matches)):
        total_hits += len(partition_matches[i])
    exact_matches, approx_matches, lowest_mismatches = check_partitions(partition_matches, reference_genome, read[1], partitions, mismatches)
    if lowest_mismatches <= mismatches: # add one to position in lowest_mismatch_list
        lowest_mismatch_list[lowest_mismatches] += 1

for val in lowest_mismatch_list:
    sys.stdout.write(str(val) + ' ')
    

